<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StampRequestTest extends TestCase
{
  use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @test
     */
    public function it_has_a_buyer()
    {
        $stamp_request = factory('App\StampRequest')->create();
        $this->assertinstanceOf('App\user', $stamp_request->buyer);
    }
}
