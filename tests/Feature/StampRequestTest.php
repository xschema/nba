<?php

namespace Tests\Feature;

use App\StampRequest;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StampRequestTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     * @test
     */
    public function authenticated_user_can_make_a_request()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->json(
                'POST',
                '/stamp-requests',
                [
                    'stamp_type' => 'Private',
                    'packs' => 4,
                    'sheets' => 1,
                    'amount' => 50000,
                    'pick_up_address' => 'Some Address',
                 ]
            );
          $response->assertStatus(201)
                  ->assertJson(['created' => true ]);
        $this->assertDatabaseHas(
            'stamp_requests',
            [ 'buyer_id' => $user->id, 'status' => 'confirmed']
        );
    }
    /**
     * A basic test example.
     *
     * @return void
     * @test
     */
    public function authenticated_user_can_view_their_requests()
    {
        $user = factory(User::class)->create();
        $stamp_requests = factory(StampRequest::class, 2)->create(['buyer_id' => $user->id]);
        $response = $this->actingAs($user)
                        ->get('/my-requests');
        $response->assertStatus(200)
                ->assertJsonCount(2, 'data');
    }

    /**
     * @test
     */
    public function a_requests_payment_status_can_be_changed_when_they_make_payment()
    {
      //someone must be signed in, either manager of the buyer
        $user = factory(User::class)->create();
        $stamp_request = factory(StampRequest::class)->create();
        $response = $this->actingAs($user)->put('/confirm-payment/'. $stamp_request->id, [
          'transaction_id' => 'TR1232374684904',
        ]);

        $response->assertStatus(201)
              ->assertJson(['confirmed' => true]);

        $this->assertDatabaseHas(
              'stamp_requests',
              [
                'status' => 'processing',
                'paid' => true,
                'transaction_id' => 'TR1232374684904',
                 'id' => $stamp_request->id ]
              );

    }

     /**
     * @test
     */
    public function a_requests_delivery_status_status_can_be_changed_by_a_manager()
    {
        $manager = factory(User::class)->create(['role' =>'manager']);
        $stamp_request = factory(StampRequest::class)->create();

        $this->assertDatabaseHas(
                              'stamp_requests',
                              ['status' => 'processing', 'id' => $stamp_request->id ]
                            );

        $response = $this->actingAs($manager)
                      ->json(
                        'PUT',
                        '/stamp-request-status',
                         [ 'stamp_request_id' => $stamp_request->id, 'status' => 'processing']
                       );


        $response->assertStatus(201)
              ->assertJson(['updated' => true]);

        $this->assertDatabaseHas(
                              'stamp_requests',
                              ['status' => 'processing', 'id' => $stamp_request->id ]
                            );

        $response = $this->actingAs($manager)
                      ->json(
                        'PUT',
                         '/stamp-request-status',
                         [ 'stamp_request_id' => $stamp_request->id, 'status' => 'shipped']);


          $this->assertDatabaseHas(
                              'stamp_requests',
                              ['status' => 'shipped', 'id' => $stamp_request->id ]
                            );
    }
    /**
     * @test
     */
    public function a_user_may_make_payment_for_a_stamp_request ()
    {
        $user = factory('App\User')->create();
        $stamp_request = factory(StampRequest::class)->create(['buyer_id' => $user->id]);
        $response = $this->actingAs( $user )
                        ->get('/stamp-request-payment/'. $stamp_request->id);
          $response->assertSee($stamp_request->packs);
    }

    /**
     * @test
     */
    public function a_user_can_see_the_invoice_for_their_request() {
        $user = factory(User::class)->create();
        $stamp_request = factory(StampRequest::class)->create(['buyer_id' => $user->id]);
        $response = $this->actingAs($user)
                        ->get('/stamp-request-invoice/'. $stamp_request->id );
        $response->assertSee($stamp_request->transaction_id);
    }
}
