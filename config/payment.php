<?php

return [
  'public' => env('PAYSTACK_PUBLIC', 'test'),
  'private' => env('PAYSTACK_PRIVATE', 'test')
];
