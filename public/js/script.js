
$(document).ready(function(){

$('.ajaxLink').on('click',function(){
    loadingModal('loading');
    var action = $(this).attr('href');

    //console.log(action);
    setTimeout(function() {
        window.location = action;
    }, 1000);
})
$('.ajaxForm-reload').on('submit', function(e){
            e.preventDefault();
            loadingModal('loading');
            $form  = $(this);
            var action = $form.attr('action');

            //console.log(action);
            var formData = $form.serialize();
            //demo.showSwal('success-message');
             $.ajax({
                type : 'POST',
                url: action,
                data : formData,
                success: function(data) {
                    //console.log(data)

                    if (data.error){//if error
                        swalModal('error', 'Error', data.error);
                        setTimeout(function() {
                            loadingModal('close')
                        }, 2000);
                        return;
                    }
                    else if(data.success){
                        swalModal('success', 'success', data.success);
                        $form[0].reset();
                        setTimeout(function() {
                            location.reload();
                        }, 2000);


                    }else{
                         swalModal('error', 'Error', 'Error Occured please try again!!');
                        setTimeout(function() {
                            loadingModal('close')
                        }, 2000);
                        return;
                    }

                },
                error: function(){
                    swalModal('error','Error','Error Occured please try again!!!');
                    setTimeout(function() {
                        loadingModal('close')
                    }, 2000);

                    //alert('Error occured, please try again!!.');
                }
                //loadingModal('close');
        });

        });
	        //post comment
        /////////////////////////////
        $('.ajaxForm').on('submit', function(e){
            e.preventDefault();
            loadingModal('loading');
            $form  = $(this);
            var action = $form.attr('action');

            //console.log(action);
            var formData = $form.serialize();

             $.ajax({
                type : 'POST',
                url: action,
                data : formData,
                success: function(data) {
                    //console.log(data)

                    if (data.error){//if error
                        swalModal('error', 'Error', data.error);
                        setTimeout(function() {
                            loadingModal('close')
                        }, 2000);
                        return;
                    }
                    else if(data.success){
                        swalModal('success', 'success', data.success);
                        $form[0].reset();
                    }else{
                         swalModal('error', 'Error', 'Error Occured please try again!!');
                        setTimeout(function() {
                            loadingModal('close')
                        }, 2000);
                        return;
                    }

                },
                error: function(){
                    swalModal('error','Error','Error Occured please try again!!!');
                    setTimeout(function() {
                        loadingModal('close')
                    }, 2000);

                    //alert('Error occured, please try again!!.');
                }
                //loadingModal('close');
        });

        });

         function loadingModal(action) {
        if(action=="loading"){
            swal({
                title: "Loading...",
                text: "Please be patient while loading your request.",
                showConfirmButton: false
            });
        }
        else{
            swal.close();
        }
    }

    function swalModal(action,title,message){
        swal({
                title: title,
                text: message,
                buttonsStyling: false,
                confirmButtonClass: "btn btn-"+action,
                type: action
            })
    }


var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = $('.img-modal');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
$('.img-modal').on('click',function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
})

// Get the <span> element that closes the modal
$('.close-modal').on('click',function(){
    modal.style.display = "none";

})
// When the user clicks on <span> (x), close the modal

});
