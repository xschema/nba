<?php

Route::get('/maintenance', 'MaintenanceController@index')->name('maintenance');

Route::get('/', 'VerificationController@index')->name('verification.index');

Route::get('/profile', 'LawyerProfileController@index')->name('frontend.profile');
Route::post('/profile/edit', 'LawyerProfileController@update')->name('profile.update');
Route::post('/profile/password', 'LawyerProfileController@changePassword')->name('profile.changePassword');


Route::get('/logout', function () {
    Auth::logout();
    return redirect()->route('verification.index');
});
//overrider defaut register
//Route::get('/register','RegistrationController@index')->name('register');

Auth::routes();

Route::get('/home', function(){
  if(!( $user = auth()->user() ) ) return redirect('/');

    $route = $user->role == 'lawyer' ? 'frontend.index' : 'backend.index';

    return redirect()->route($route);

});

Route::get('/manager', 'BackendController@index')->name('backend.index');
Route::get('/manager/all-requests', 'BackendController@showPaginatedRequests')->name('all-requests');
Route::get('/all-requests-data', 'BackendController@AllRequests')->name('all-requests-data');
Route::post('/export-requests', 'BackendController@exportRequests');
Route::get('/export-reports', 'BackendController@exportReports');
// Route::get('/');
Route::get('/exports/today', 'BackendController@exportToday')->middleware('auth');

Route::get('/terms-and-conditions', 'TermConditionController@index')->name('terms-conditions');

require_once 'stamp-request-routes.php';
require_once 'profile-routes.php';
require_once('ticket-routes.php');
require_once('verification-routes.php');
require_once('ticket-routes.php');
require_once('backend-ticket-routes.php');
require_once('registration-routes.php');
require_once('get-lawyer-routes.php');
