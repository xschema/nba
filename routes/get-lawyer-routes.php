<?php
Route::get('get-lawyer-data', 'GetLawyerController@getlawyerData')->name('getlawyer.data');

Route::resources([
    'getLawyer'=>'GetLawyerController',
]);