<?php

Route::get('/manager/ticket', 'ManagerTicketController@index')->name('backend.ticket.index');
Route::get('/manager/ticket/{query}', 'ManagerTicketController@index')->name('backend.ticket.filter');
Route::get('/manager/ticket/{t_num?}/{id?}', 'ManagerTicketController@show')->name('backend.ticket.show');
Route::post('/manager/ticket/close/{id}', 'ManagerTicketController@close')->name('backend.ticket.close');
