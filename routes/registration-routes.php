<?php

/*Route::resources([
    'registration'=>'RegistrationController',
]);*/

Route::get('/registration','RegistrationController@index')->name('registration.index');

Route::post('/registration','RegistrationController@store')->name('registration.store');

Route::get('/registration/{hash?}/{id?}/','RegistrationController@show')->name('registration.show');

Route::patch('/registration/{id?}','RegistrationController@update')->name('registration.update');

// Route::get('/registration/edit/{hash?}/{id?}', 'RegistrationController@edit')->name('registration.edit');

//Route::get('/view-registration/', 'GetRegistrationController@index')->name('backend-registration');
Route::get('/view-registration', 'GetRegistrationController@index')->name('backend-registration');
Route::get('/view-registration/status/{status?}', 'GetRegistrationController@index')->name('backend-registration.status');
Route::get('/view-registration/{id}', 'GetRegistrationController@show')->name('backend-registration.show');
Route::get('/view-registration/action/{id?}/{action?}', 'GetRegistrationController@approve')->name('backend-registration.approve');
Route::post('/view-registration/action','GetRegistrationController@reject')->name('backend-registration.reject');