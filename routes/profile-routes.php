<?php
Route::get('/profile', 'LawyerProfileController@index')->name('frontend.profile');
Route::post('/profile/edit', 'LawyerProfileController@update')->name('profile.update');
Route::post('/profile/password', 'LawyerProfileController@changePassword')->name('profile.changePassword');

