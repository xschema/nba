<?php

//'ticket'=>'TicketController'
Route::get('/ticket', 'TicketController@index')->name('ticket.index');
Route::get('/ticket/open', 'TicketController@create')->name('ticket.create');
Route::get('/ticket/{t_num}/{id}', 'TicketController@show')->name('ticket.show');
Route::post('/ticket', 'TicketController@store')->name('ticket.store');

Route::post('/ticket/reply', 'ReplyController@store')->name('ticket.reply');
