<?php
Route::get('/index', 'FrontendController@index')->name('frontend.index')->middleware('auth');
Route::get('/requests', 'FrontendController@buyerRequests')->name('buyer-requests');

Route::resource('stamp-requests', 'StampRequestController');
Route::get('/my-requests', 'StampRequestController@buyerRequests')->name('my-requests');
Route::put('/confirm-payment/{stamp_request}', 'StampRequestController@confirmPayment');
Route::put('/stamp-request-status', 'StampRequestController@updateDeliveryStatus');
Route::get('/stamp-request-payment/{stamp_request}', 'StampRequestController@showPayment')->name('payment-url');
Route::get(' /stamp-request-invoice/{stamp_request}', 'StampRequestController@showInvoice')->name('invoice-url');
