<?php

// Route::get('/index', function () {

//     return view('frontend.index');

// })->name('frontend.index');

//get from nba db
Route::post('/nba', 'NbaDbController@show')->name('nba.show');

Route::get('/', 'VerificationController@index')->name('verification.index');

Route::post('/verify-me', 'VerificationController@verify')->name('verification.verify');

Route::resources([
    'verification'=>'VerificationController',
]);
