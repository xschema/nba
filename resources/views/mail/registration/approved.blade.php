@component('mail::message')
{{$message_body}}

@component('mail::button', ['url' => $url])
Login
@endcomponent

Regards,<br>
{{ config('app.name') }}
@endcomponent
