@component('mail::message')
Hi {{$name}}, <br>
{{$message_body}}

@component('mail::button', ['url' => $url])
Click here
@endcomponent

Regards,<br>
{{ config('app.name') }}
@endcomponent
