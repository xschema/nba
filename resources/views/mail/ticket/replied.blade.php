@component('mail::message')

Your Support Ticket entitiled "{{$subject}}" has recieved a response. Please click on the Ticket Address below to log into your account to view the response.

@component('mail::button', ['url' => $url])
View Ticket
@endcomponent

Regards,<br>
{{ config('app.name') }}
@endcomponent
