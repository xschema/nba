@component('mail::message')
{{$message_body}}

@component('mail::button', ['url' => $url])
View Ticket
@endcomponent

Regards,<br>
{{ config('app.name') }}
@endcomponent
