<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NBA Stamp</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="vendor/landing-page.css" rel="stylesheet">
     @stack('styles')
  </head>
  <body>

      @yield('content')

      <!-- Footer -->
    <footer class="footer bg-light" >
      <div class="container">
        <div class="row">
          <div class="col-lg-4 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">

              {{-- <li class="list-inline-item">&sdot;</li> --}}
              {{-- <li class="list-inline-item">
                <a href="#">Contact</a>
              </li> --}}
              <li class="list-inline-item">
                <a href="{{route('terms-conditions')}}" target="_blank">Terms and Conditions</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&copy;2018. Nigerian Bar Association </p>
          </div>

          <div class="col-lg-4 h-100 text-center my-auto">
            <h4>Disclaimer</h4>
            <p class="text-muted small">This site is exclusively for members of NBA.</p>
          </div>

          <div class="col-lg-4 h-100 text-center text-lg-right my-auto">
            <p class="text-muted small">Designed and Powered by <a href="#">Strataflex</a></p>
          </div>

        </div>
      </div>
    </footer>
<script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    @stack('scripts')
</body>

</html>
