@extends('layouts.frontend.landing')
@push('styles')

@endpush


@section('content')
<!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="{{ route('verification.index') }}">
          <img src="{{ asset('img/logo.png') }}" width="70" class="img-responsive">
         </a>     


      </div>
    </nav>

    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
          	
            <h3 class="mb-5">Site Under Scheduled Maintenance</h3>
            <p>Sorry for the inconvenience.</p>
            <p>We will be back up and running as soon as possible</p>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
          
      			  
          </div>
        </div>
      </div>
    </header>
@endsection

@push('scripts')

@endpush



