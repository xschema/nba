@extends('layouts.frontend.main')
@section('title','Request')
@prepend('styles')
 <!-- page css -->
    <link href="{{ asset('assets/node_modules/wizard/steps.css')}}" rel="stylesheet">
    <link href="/assets/node_modules/wizard/steps.css" rel="stylesheet">
    <script>
      window.user = {!! $buyer !!}
      window.branches = {!! $branches !!}
    </script>
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endprepend

  @section('content')
  <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Stamp Request</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">stamp Request</li>
                </ol>
            </div>
        </div>
  </div>
   <div id="app">
      <flash message="{{ session('message') }}"></flash>
      <new-stamp-request> </new-stamp-request>
   </div>
@endsection

@push('scripts')
 <!--morris JavaScript -->
    <script src="{{ asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('dist/js/custom.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/wizard/jquery.steps.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/wizard/jquery.validate.min.js')}}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/wizard/steps.js')}}"></script>
    <script src="{{asset('js/manifest.js')}}"></script>
    <script src="{{asset('js/vendor.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>

@endpush
