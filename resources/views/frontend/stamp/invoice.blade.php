@extends('layouts.frontend.main')
@section('title','Stamp Request Invoice')
@prepend('styles')
 <!-- page css -->
@endprepend
  @section('content')
  <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Stamp Request Payment Invoice</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Stamp Request</li>
                </ol>
            </div>
        </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-body printableArea">
        <h3><b>INVOICE</b> <span class="pull-right">{{ $stamp_request->transaction_id}}</span></h3>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <div class="pull-left">
              <address>
                <h3> &nbsp;<b class="text-danger">Nigerian Bar Association</b></h3>
                <p class="text-muted m-l-5" style="line-height: 1.9"> NATIONAL SECRETARIAT: NBA House,
                  <br> Plot 1101 Mohammadu Buhari Way,
                  <br> Central Business District, Abuja F.C.T. Nigeria,
                  <br>Email: info@nigerianbar.org.ng
                  <br>Phone: +234810 402 5812</p>
                </address>
              </div>
              <div class="pull-right text-right">
                <address>
                  <h3>To,</h3>
                  <h6 class="font-bold">{{ $stamp_request->buyer->name}}</h6>
                  <p class="font-medium">{{ $stamp_request->pick_up_address}}</p>
                    <p><b>Invoice Date :</b> <i class="fa fa-calendar"></i> {{ $stamp_request->created_at->toFormattedDateString()}}</p>
                  </address>
                </div>
              </div>
              <div class="col-md-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th>Description</th>
                        <th class="text-center">Packs</th>
                        <th class="text-center">Sheets</th>
                        <th class="text-center">Charges</th>
                        <th class="text-right">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td>Request for <strong>{{$stamp_request->type }}</strong> Stamps </td>
                        <td class="text-center">{{$stamp_request->packs }} &times; {{naira(4000)}}</td>
                        <td class="text-center"> {{$stamp_request->sheets }} &times; {{naira(1500)}} </td>
                        <td class="text-center"> {{naira($stamp_request->charges/100) }}</td>
                        <td class="text-right"> {{naira(($stamp_request->total_cost/ 100)) }}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-12">
                <div class="pull-right m-t-30 text-right">
                 {{--  <p>Sub - Total amount: $13,848</p>
                  <p>vat (10%) : $138 </p> --}}
                  <hr>
                  <h3><b>Total :</b> {{naira(($stamp_request->total_cost / 100)) }} </h3>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="text-right">
                  @if(!$stamp_request->isPaid)
                  <a href="{{ route('payment-url', $stamp_request) }}">  <button class="btn btn-danger" type="submit"> Proceed to payment </button> </a>
                  @endif
                  <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@push('scripts')
<script src="{{ asset('dist/js/pages/jquery.PrintArea.js')}}" type="text/JavaScript"></script>
    <script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>
@endpush
