@extends('layouts.frontend.main')
@section('title','Request')
@prepend('styles')
 <!-- page css -->
    <script src="https://js.paystack.co/v1/inline.js"></script>
    <style>
    /* .paystack-container .test-mode-badge{
    position:inherit !important;
} */
</style>
@endprepend

  @section('content')
  <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Stamp Request Payment</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Stamp Request</li>
                </ol>
            </div>
        </div>
  </div>
  <div class="row">
    <div class="col-sm-5 mx-auto"   >
      <div id="paystackEmbedContainer"></div>
    </div>
  </div>
<flash></flash>
@endsection
@push('scripts')
<script src="{{ asset('js/axios.js')}}"></script>
<script>
  PaystackPop.setup({
   key: "{{ config('payment.public') }}",
   email: "{{ $stamp_request->buyer->email }}",
   name: "{{ $stamp_request->buyer->name }}",
   amount: {{ $stamp_request->total_cost }},
   metadata: {
        request_id: {{$stamp_request->id}},
        custom_fields: [
          {
            display_name: "Paid on",
            variable_name: "paid_on",
            value: 'NBA Stamp Application'
          },
          {
            display_name: "Number of Packs",
            variable_name: 'packs',
            value: {{ $stamp_request->packs }}
          },
          {
            display_name: "Number of Sheets",
            variable_name: 'sheets',
            value: {{ $stamp_request->sheets }}
          },
          {
            display_name: "Stamp Type",
            variable_name: 'type',
            value: "{{ $stamp_request->type }}"
          },
          {
            display_name: "Delivery Address",
            variable_name: 'delivery_address',
            value: "{{ $stamp_request->pick_up_address }}"
          }
        ]
      },
   container: 'paystackEmbedContainer',
   callback: function(response){
        if( ! ( response.status.toLowerCase() === 'success') ) {
         flash('Opps!, there was a problem processing your payment, please contact support')
         return
       }
       // confirm payment,
        axios.put('/confirm-payment/{{ $stamp_request->id }}', { transaction_id : response.reference })
              .then( resp => {
                if ( resp.data.confirmed === true) {
                  window.location.pathname = `/stamp-request-invoice/{{ $stamp_request->id }}`
                }
              } )
       // got to invoice page (where we also send notification and drop one in the db)
    },
  });
</script>
@endpush
