@extends('layouts.frontend.main')
@section('title','Request')
@prepend('styles')
 <!-- page css -->
    <link href="{{ asset('assets/node_modules/wizard/steps.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endprepend

@section('content')
	<div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Stamp Request</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">stamp Request</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <div class="card-body wizard-content">
                    <h4 class="card-title text-center">Request For Stamp</h4>
                    <h6 class="card-subtitle text-center mb-4">Please fill all fields correctly</h6>
                    <form action="#" class="tab-wizard wizard-circle mt-3">
                        <!-- Step 1 -->
                        <h6>Personal Information</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstName1">First Name :</label>
                                        <input type="text" class="form-control" id="firstName1" value="{{ auth()->user()->first_name}}" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lastName1">Last Name :</label>
                                        <input type="text" class="form-control" id="lastName1" value="{{ auth()->user()->last_name}}" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="emailAddress1">Email Address :</label>
                                        <input type="email" class="form-control" id="emailAddress1" value="{{ auth()->user()->email}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phoneNumber1">Phone Number :</label>
                                        <input type="tel" class="form-control" id="phoneNumber1" value="{{ auth()->user()->lawyer_account->phone_number }}"> </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="location1">Branch</label>
                                        <select class="custom-select form-control" id="location1" name="location">
                                           @foreach($branches as $branch)
                                                <option value="{{$branch->branch}}" {{ $branch->branch === auth()->user()->lawyer_account->branch? 'selected' : '' }}> {{ $branch->branch }}</option>
                                           @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </section>
                        <!-- Step 2 -->
                        <h6>Stamp Details</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="intType1">Stamp Type</label>
                                        <select class="custom-select form-control" id="stamptype" data-placeholder="Stamp Type" name="stamp-type">
                                            <option value="Private">Private</option>
                                            <option value="Public">Public</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    	<div class="form-group">
	                                        <label for="intType1">Quantity (packs):</label>

                                    	</div>
                                	</div>
                                	 <div class="col-md-6">
	                                    <div class="form-group">
	                                        <!-- <label for="intType1">Quantity:</label> -->
	                                        <select class="custom-select form-control" id="intType1" data-placeholder="Stamp Type" name="intType1">
	                                            <option value="1">1</option>
	                                            <option value="2">2</option>
	                                        </select>
	                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    	<div class="form-group">
	                                        <label for="intType1">Quantity (Sheets):</label>
	                                        <!-- <select class="custom-select form-control" id="intType1" data-placeholder="Stamp Type" name="intType1">
	                                            <option value="Private">Pack</option>
	                                        </select> -->
                                    	</div>
                                	</div>
                                	 <div class="col-md-6">
	                                    <div class="form-group">
	                                       <!--  <label for="intType1">Quantity (Sheets):</label> -->
	                                         <select class="custom-select form-control" id="intType1" data-placeholder="Stamp Type" name="intType1">
	                                            <option value="1">1</option>
	                                            <option value="2">2</option>
	                                        </select>
	                                    </div>

                                </div>

                            </div>

                        </section>
                        <!-- Step 3 -->
                        <h6>Checkout</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                	<h3>Checkout</h3>
                                	<table class="table">
                                		<thead>
                                			<tr>
                                				<th>Item</th>
                                				<th>Item</th>
                                			</tr>
                                		</thead>
                                		<tbody>
                                			<tr>
                                				<td>Stamp Type</td>
                                				<td>Public</td>
                                			</tr>
                                			<tr>
                                				<td>Pack(s)</td>
                                				<td>3</td>
                                			</tr>
                                			<tr>
                                				<td>Sheets(s)</td>
                                				<td>2</td>
                                			</tr>
                                			<tr class="table-danger">
                                				<td >Total  </td>
                                				<td>#15,000</td>
                                			</tr>
                                		</tbody>
                                	</table>
                                </div>
                            </div>
                        </section>
                        <!-- Step 4 -->
                        <h6>Pick up</h6>
                        <section>
                    <div class="row">
                        <div class="form-group">
								<label class="control-label">Pick Up Location</label>
								<div class="custom-control custom-radio">
									<input type="radio" id="my_branch" name="pickup" class="custom-control-input branch_office">
									<label class="custom-control-label" for="my_branch">My Branch</label>
						         </div>

                                 <div class="custom-control custom-radio">
                                    <input type="radio" id="national_office" name="pickup" class="custom-control-input branch_office">
                                    <label class="custom-control-label" for="National_office">National Office</label>
                                 </div>

                    <div class="custom-control custom-radio">
	                   <input type="radio" id="my_location" name="pickup" class="custom-control-input">
	                   <label class="custom-control-label" for="my_location">Another location</label>

                    </div>
        </div>
        <div class="col-md-12">
		  <textarea id="new_address"  name="new_address" class="form-control" placeholder="Pickup Address" style="display: none;"></textarea>
    	</div>
                    </div>
                        </section>
                    </form>
                </div>
            </div>
        </div>

         <div class="col-sm-12 col-md-6">
        	<div class="card">
        		<div class="card-body wizard-content">
                    <h4 class="card-title text-right">#20,000</h4>

                    <div style="margin-left: 20%">
                    	<img src="{{asset('img/private.jpg')}}" class="img-responsive" width="200px">
                    </div>

                    <div class="" style="margin-top: 20px; margin-left: 20%">
                    	<ul style="list-style: none; font-size: 16px;">
                    		<li>Stamp Type: Private</li>
                    		<li>Pack(s): 2</li>
                    		<li>Sheet(s): 3</li>
                    	</ul>
                    </div>
                </div>
        	</div>
         </div>
    </div>

@endsection

@push('scripts')
<!--morris JavaScript -->
    <script src="{{ asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('dist/js/custom.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/wizard/jquery.steps.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/wizard/jquery.validate.min.js')}}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/wizard/steps.js')}}"></script>

     <script type="text/javascript">
     	$('#my_location').click(function(e){
     		if($(this).is(':checked')){
     			$('#new_address').prop('required',true);
     			$('#new_address').show('slow');
     		}
     	});

     	$('.branch_office').click(function(e){
     		if($(this).is(':checked')){
     			$('#new_address').removeAttr('required');
     			$('#new_address').hide('slow');
     		}
     	});
     </script>
     <script >
       $(document).ready(function(){
        const StampApp = {

        }
        const $stampTypeSelect = $('#stamp-type')
       })
     </script>
@endpush
