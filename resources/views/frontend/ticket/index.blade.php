@extends('layouts.frontend.main')
@section('title','Ticket')
@push('styles')
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/node_modules/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css')}}" />
<link href="dist/css/pages/inbox.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<style type="text/css">
  .label-default {
    background-color: #737980;
}
    .list-group .badge{
        float:right;
    }
    .pagination{
        float: right;
    }
</style>
@endpush

@section('content')

 <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ticket</h4>
    </div>

    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('frontend.index')}}">Home</a></li>
                <li class="breadcrumb-item active">Ticket</li>
            </ol>
            <a href="{{route('ticket.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Open Ticket</a>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="card-body inbox-panel">

                        <a href="{{route('ticket.create')}}" class="btn btn-success m-b-20 p-10 btn-block waves-effect waves-light">Open Ticket</a>

                        <ul class="list-group list-group-full">
                            <li class="list-group-item"><i class="mdi mdi-gmail"></i> Total<span class="badge badge-success ml-auto">{{$total}}</span></li>

                             <li class="list-group-item"> <i class="mdi mdi-gmail"></i> Pending
                             	<span class="badge badge-danger ml-auto">{{$pending}}</span>
                             </li>

                             <li class="list-group-item"> <i class="mdi mdi-gmail"></i> Aswered
                             	<span class="badge badge-info ml-auto">{{$answered}}</span>
                             </li>
                             <li class="list-group-item"> <i class="mdi mdi-gmail"></i> Closed
                                <span class="badge badge-default ml-auto">{{$closed}}</span>
                             </li>
                        </ul>

                    </div>
                </div>

                <div class="col-lg-9 col-md-8 bg-light border-left">
                     @if(count($tickets)>0)
                    <div class="card-body">
                        <div class="col-md-12">
                            <h3>My Tickets</h3>
                            {{$tickets->links()}}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="card-body p-t-0 show-content">
                        <div class="card b-all shadow-none">
                            <div class="inbox-center table-responsive">
                                <table class="table table-hover no-wrap">
                                    <tbody>

                                            @foreach($tickets as $ticket)
                                                @php

                                                 $status = ($ticket->status =='New') ? 'Pending' : $ticket->status;

                                                $subject = $ticket->subject;
                                                $date = $ticket->updated_at->diffForHumans();
                                                $attachment = '';
                                                //check if has attachment
                                                if(count($ticket->attachment)< 1 )
                                                    $attachment = 'hide';
                                                @endphp

                                                <tr class="{{$status}}}">

                                                    <td class="max-texts">
                                                        <a href="{{route('ticket.show',[$ticket->ticket_number,$ticket->id])}}">
                                                             {{$subject}}

                                                         </a>
                                                    </td>
                                                    <td>
                                                        <span class="label label-{{$status}}">{{$status}}</span>
                                                             <i class="fa fa-paperclip {{$attachment}}"></i>
                                                    </td>
                                                    <!-- <td class="hidden-xs-down"><i class="fa fa-paperclip"></i></td> -->
                                                    <td class="text-right"> {{$date}} </td>
                                                </tr>

                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="card-body show-content">
                        <p style="padding-top: 20px; text-align: center;">No Ticket Found</p>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<!--morris JavaScript -->
    <script src="{{ asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
     <script src="{{asset('assets/node_modules/dropzone-master/dist/dropzone.js')}}"></script>
    <script type="text/javascript">
    	$(document).ready(function(){

            $('.label-New').addClass('label-success');
            $('.label-Pending').addClass('label-danger');
            $('.label-Answered').addClass('label-info');
            $('.label-Closed').addClass('label-default');

    		$('.ajaxLink').on('click',function(e){
    			e.preventDefault();

    			var view_url = $(this).attr('href');

    			swal({
                title: "Loading...",
                text: "Please be patient while loading your request.",
                showConfirmButton: false
            });

		        $.ajax({
		            url: view_url,
		            type: 'get',

		            success:function(response) {
		                setTimeout(function(){
		                    $('.bg-light').html(response);
		                    swal.close();
		                    }, 600);
		            },
		            error: function(){

		                swal.close();
		            }
		        });
    		});

            /*$('#new-ticket').on('click', function(e){
                e.preventDefault();
                //var url = $(this).attr('href');
            });*/
    	});
    </script>
@endpush
