@extends('layouts.frontend.main')
@section('title',"Ticket: $ticket->ticket_number")
@push('styles')

    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">

<link href="{{ asset('dist/css/pages/inbox.css')}}" rel="stylesheet">
<link href="{{ asset('css/style.css')}}" rel="stylesheet">

<style type="text/css">
    .list-group .badge{
        float:right;
    }
    .pagination{
        float: right;
    }
</style>
@endpush

@section('content')

 <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ticket</h4>
    </div>

    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('frontend.index')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('ticket.index')}}">My Ticket</a></li>
                <li class="breadcrumb-item active">View Ticket</li>
            </ol>
            <a href="{{route('ticket.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Open Ticket</a>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="card-body inbox-panel">

                        <a href="{{route('ticket.create')}}" class="btn btn-success m-b-20 p-10 btn-block waves-effect waves-light">Open Ticket</a>

                        <ul class="list-group list-group-full">
                            <li class="list-group-item active">
                                <a href=""><i class="mdi mdi-gmail"></i> Total </a>
                                <span class="badge badge-success ml-auto">{{$total}}</span>
                            </li>

                             <li class="list-group-item">
                                <a href=""><i class="mdi mdi-gmail"></i> Pending </a>
                                <span class="badge badge-danger ml-auto">{{$pending}}</span>
                             </li>

                             <li class="list-group-item">
                                <a href=""><i class="mdi mdi-gmail"></i> Aswered </a>
                                <span class="badge badge-info ml-auto">{{$answered}}</span>
                             </li>
                             <li class="list-group-item">
                                <a href=""><i class="mdi mdi-gmail"></i> Closed </a>
                                <span class="badge badge-default ml-auto">{{$closed}}</span>
                             </li>

                        </ul>

                    </div>
                </div>
                @php
                //change new tickets to Pending
                    $status = ($ticket->status =='New') ? 'Pending' : $ticket->status;
                    $image = $ticket->user->avatar_url;
                @endphp
                <div class="col-lg-9 col-md-8 bg-light border-left">

                    <div class="card b-all shadow-none">
                        <div class="card-body">

                             @if($flash = session('alert'))
                            <div class="col-md-12" >
                                <div class="row mb-4 pt-2 pl-2" id="flash-message">
                                       
                                            @include('partials.alert')
                                       
                                </div>
                            </div> 
                            @endif

                            <h4 class="card-title m-b-0">{{$ticket->subject}} <small>Ticket: #{{$ticket->ticket_number}}</small></h4>
                        </div>
                        <div>
                            <hr class="m-t-0">
                        </div>
                        <div class="card-body">
                            <div class="d-flex m-b-20">
                                <div>
                                    <img src="{{asset($image)}}" alt="user" width="40" class="img-circle" />
                                </div>
                                <div class="p-l-10">
                                    <h4 class="m-b-0">{{$ticket->user->name}}</h4>
                                    <small class="text-muted">From: Me</small> &nbsp;&nbsp; <span class="label label-{{$status}}">{{$status}}</span>
                                </div>
                            </div>
                            <p>{{$ticket->message}}</p>

                        </div>

                        <div>
                            <hr class="m-t-0">
                        </div>

                        @if(count($ticket->attachment)>0)
                            <div class="card-body">

                                <h4><i class="fa fa-paperclip m-r-10 m-b-5"></i> Attachments <span>({{count($ticket->attachment)}})</span></h4>

                                <div class="row">
                                    @foreach($ticket->attachment as $attachment)
                                    <div class="col-md-2">
                                         <img class="img-thumbnail img-responsive img-modal" alt="attachment" src="{{asset($attachment->path)}}">
                                    </div>
                                    @endforeach
                                </div>

                            </div>

                              <div>
                                <hr class="m-t-0">
                            </div>
                        @endif

                         @if(count($ticket->reply)>0)
                            <div class="card-body" style="background-color: #eceaea33;">
                            <h4>Replies</h4>
                                <div class="profiletimeline">

                                     @foreach($ticket->reply as $reply)
                                         @php
                                         $name = "Staff";
                                            if($reply->user_id == Auth::user()->id){
                                            $name ='Me';
                                        }
                                         @endphp
                                    <div class="sl-item">
                                        <div class="sl-left">
                                            <img src="{{ asset($image)}}" alt="user" class="img-circle" />
                                        </div>
                                        <div class="sl-right">
                                            <div>
                                                <span class="link">{{$name}}</span>&nbsp;&nbsp; <span class="sl-date text-right" > {{$reply->created_at->diffForHumans()}}</span>

                                                <p>{{$reply->message}} </p>



                                                <div class="row">
                                                    @if(count($reply->attachment)>0)
                                                        @foreach($reply->attachment as $attachment)
                                                        <div class="col-md-2">
                                                             <img class="img-thumbnail img-responsive img-modal" alt="attachment" src="{{asset($attachment->path)}}">
                                                        </div>
                                                         @endforeach
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        @endif

                            <div>
                                <hr class="m-t-0">
                            </div>
                            @if($ticket->status !='Closed')
                        <div class="card-body">
                            <button class="btn btn-warning" id="reply-button"><i class="mdi mdi-reply"></i> Reply</button>
                             
                            <div class="hide" id="reply-form">
                            <h4>Reply</h4>
                            <form action="{{route('ticket.reply')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <textarea class="textarea_editor form-control" name="message" rows="5" placeholder="Enter text ..."></textarea>
                                </div>
                                <input type="hidden" name="ticket_number" value="{{$ticket->ticket_number}}">

                                <h4><i class="ti-link"></i> Attachment</h4>
                            <span class="dropzone">
                                <div class="fallback">
                                    <input name="attached[]" type="file" multiple />
                                </div>
                            </span>
                            <br />

                                <button type="submit" class="btn btn-success">
                                    Send
                                </button>
                            </form>
                        </div>
                        </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close-modal">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>

@endsection

@push('scripts')
<!--morris JavaScript -->
    <script src="{{ asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/script.js')}}"></script>
     <!-- <script src="{{asset('assets/node_modules/dropzone-master/dist/dropzone.js')}}"></script> -->
    <script type="text/javascript">

        $(document).ready(function(){
         setTimeout(function(){
          $('#flash-message').hide('slow')
        }, 3000)


            $('.label-New').addClass('label-success');
            $('.label-Pending').addClass('label-danger');
            $('.label-Answered').addClass('label-info');
            $('.label-Closed').addClass('label-default');

            $('#reply-button').on('click',function(e){
                $('#reply-form').toggle('slow');
            });
        });
    </script>
@endpush
