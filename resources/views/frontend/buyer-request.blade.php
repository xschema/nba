@extends('layouts.frontend.main')
@section('title','Dashboard')
@prepend('styles')
<!-- Custom CSS  -->
<link href="assets/node_modules/morrisjs/morris.css" rel="stylesheet">
<!--Toaster Popup message CSS -->
<link href="assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
<style>
  .label-processing {
    background-color: #fb9678;
    border-color: #fb9678;
    border-color: #dee2e6;
  }

  .label-confirmed,
  .label-unconfirmed {
    color: #212529;
    background-color: #f8f9fa;
  }
  .label-shipped {
    background-color: #00c292;
    border-color: #00c292;
  }
  .label-paid {

  }
  .label-unpaid {

  }
</style>
@endprepend

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
  <div class="col-md-5 align-self-center">
    <h4 class="text-themecolor">My Request</h4>
  </div>
  <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>
    </div>
  </div>
</div>
<div class="col-12" id="app">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">My Requests</h4>
      <h6 class="card-subtitle mt-2 mb-4"> This is a listing of all your requests, from here you can make payments for a purchase, view the invoice/reciept for a purchase or view its status.</h6>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Details</th>
              <th>Status</th>
              <th>Date</th>
              <th class="text-nowrap">Action</th>
            </tr>
          </thead>
          <tbody>
            @forelse($stamp_requests as $request)
            <tr>
              <td>
                {{$request->details}}
                <h6 class="text-success text-bold pull-right">
                  {{naira($request->amount/100)}}
                  <small>
                    (&plus; {{naira($request->charges/100)}} charges )
                  </small>
                </h6>
              </td>
              <td>
                <span class="label label-{{ $request->paid ? 'success' : 'danger'}}">{{ $request->paid ? 'paid' : 'unpaid'}}</span>
                <span class="label label-{{$request->status}}">{{$request->status}}</span>
              </td>
              <td>{{ $request->created_at->toFormattedDateString()}}</td>
              <td class="text-nowrap">
                <a href="{{ $request->invoice_url}}" data-toggle="tooltip" data-original-title="View Invoice"> <i class="mdi mdi-ticket-confirmation text-inverse m-r-10"></i> </a>
                @unless($request->paid)
                <a href="{{ $request->payment_url}}" data-toggle="tooltip" data-original-title="Make Payment"> <i class="mdi mdi-wallet text-success"></i> </a>
                @endunless
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="4" class="text-center h3 font-weight-bold">Sorry, there are no requests at this time</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">
          <i class="mdi mdi-cart text-success"></i>
          My Cart
          </h4>
        <h6 class="card-subtitle mt-2 mb-4">
          This shows your uncompleted requests.
        </h6>
          @if($cart)
            <app-cart :request="{{$cart}}" />
          @endif
        </div>
      </div>
      <flash message="{{ session('message') }}"></flash>

  </div>
</div>
  @endsection

  @push('scripts')
    <script src="{{asset('js/manifest.js')}}"></script>
    <script src="{{asset('js/vendor.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
  @endpush
