@extends('layouts.frontend.landing')
@push('styles')
<style type="text/css">
	p{
		text-align: justify;
	}
</style>
@endpush


@section('content')
   <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="{{ route('verification.index') }}">
          <img src="{{ asset('img/logo.png') }}" width="70" class="img-responsive">
         </a>

        <div class="navbar-right">
          <a href="{{route('verification.index')}}" class="btn btn-info"> Home</a>
        </div>
        


      </div>
    </nav>

    <div class="container" style="min-height: 400px; margin-top: 50px;">
    	<div class="row">
    		<div class="col-md-8 col-lg-offset-2">

    			<h2>Terms and Conditions</h2>
    			<div class="col-lg-12">
    				<h3>Privacy Policy</h3>
    				<p>
    					nbaportal.org.ng respects and protects the privacy of the individuals that access the information and use the services provided through them. Individually identifiable information about the User is not willfully disclosed to any third party without first receiving the User's permission, as covered in this Privacy Policy.
    				</p>
    				<p>
    					This Privacy Policy also covers our treatment of personally identifiable information collected from Sub-Merchants who use or may want to use our payment gateway services, as well as consumer information that we acquire in the course of our business. This Policy also covers the treatment of personally identifiable information that nbaportal.org.ng's back end technology providers / partners may share with it.

    				</p>
    			</div>

    			<div class="col-lg-12">
    				<h3>Disclaimers</h3>
    				<h4> 1. Terms Of Use - General    					
    				</h4>
    				<p>
    					Access to and use of nbaportal.org.ng is provided subject to these conditions. Use of this Site constitutes acceptance of these conditions. If these conditions are not accepted fully, the use of this Site must be terminated immediately.
    				</p>
    				<P>
    					nbaportal.org.ng has made reasonable efforts to secure accuracy of the information provided at this Site and accepts no responsibility/liability for any inadvertent inaccuracies or omissions in this Site. nbaportal.org.ng is at liberty to make changes in the products & services. As far as practicable, reasonable notice of the same will be posted on the site.
    				</P>
    				<P>
    					nbaportal.org.ng does not warrant or represent that the information or materials (including but not limited to the documents and related graphics) available at or from this site are accurate or suitable or reliable or that the site will be free of errors, viruses, bugs, problems or other limitations.
    				</P>
    				<p>
    					Users agree and undertake not to disturb the normal operation of this Site, not to infringe the integrity of this Site by hacking or altering the information contained in this Site, not to prevent or limit access to this Site to other Users, or otherwise.
    				</p>

    			</div>

    			<div class="col-lg-12">
    				<h3>2. Use Of Services</h3>
    				<p>
    					The registered Users may use the services to access any permitted User Account & to conduct transactions as may be permitted with respect to any such User Account. The User agrees to follow instructions of nbaportal.org.ng in effect from time to time with respect to use of the Services.
    				</p>

    				<p>
    					When a user account is used to conduct any Transaction, the authorization given at the time of the Transaction will be treated as if it was given by the User in person, and the User agrees to be bound by each such transaction.
    				</p>
    			</div>

    			<div class="col-lg-12">
    				<h3>3. Transaction Verifications And Records</h3>
    				
    				<p>
    					nbaportal.org.ng's records of each Transaction, and its accounting records, will be deemed to be correct, and will be conclusive and binding upon the User. If the User believes that nbaportal.org.ng records contain an error or omission, the User must give written notice of the suspected error or omission to nbaportal.org.ng within 15 days of the Transaction date.
    				</p>
    			</div>


    		</div>
    	</div>
    </div> 
@endsection