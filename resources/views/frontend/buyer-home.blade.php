@extends('layouts.frontend.main')
@section('title','Dashboard')
@prepend('styles')
<!-- Custom CSS  -->
    <link href="assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
@endprepend

@section('content')
<!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">My Dashboard</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->



                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3>
                                            <p class="text-muted">My Stamp Requests</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary">{{ $stamp_requests->count() }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3>
                                            <p class="text-muted">Unpaid Requests</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-danger">{{ $stamp_requests->where('paid', false)->count() }}</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $percentage}}%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text-outline"></i></h3>
                                            <p class="text-muted"> My Tickets </p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-info"> {{$totalTickets}}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text"></i></h3>
                                            <p class="text-mute">Pending Tickets</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-warning">{{$pendingTickets}}</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 50%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- Comment - table -->
                <!-- ============================================================== -->
                <div class="row">

                    <!-- ============================================================== -->
                    <!-- Table -->
                    <!-- ============================================================== -->
                  <div class="col-sm-8">
                   <div class="card border-success">
                            <div class="card-header bg-success">
                                <h4 class="m-b-0 text-white">Quick Actions</h4></div>
                            <div class="card-body">
                                <h3 class="card-title">Welcome to NBA Stamps Purchase Platform</h3>
                                <p class="card-text mb-5">From here you can quicky navigate through the system.</p>
                                <p class="card-text text-center mt-3 text-uppercase">What would you like to do now ?</p>
                                <div class="row button-group mx-auto mb-5 pt-5">
                                    <div class="col-sm-6 col-lg-3">
                                      <a href="{{ route('stamp-requests.create')}}">
                                         <button type="button" class="btn btn-block btn-outline-success">Buy Stamps</button>
                                      </a>

                                    </div>
                                    <div class="col-sm-6 col-lg-3">
                                        <a href="{{ route('buyer-requests') }}">
                                          <button type="button" class="btn btn-block btn-outline-success">Make Payment</button>
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-lg-3">
                                         <a href="{{ route('buyer-requests') }}">
                                          <button type="button" class="btn btn-block btn-outline-success">See my Requests</button>
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-lg-3">
                                         <a href="{{ route('ticket.create') }}">
                                          <button type="button" class="btn btn-block btn-outline-success">Open Ticket</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="{{ auth()->user()->avatar_url }}" class="img-circle" width="150">
                                    <h4 class="card-title m-t-10">{{auth()->user()->name}}</h4>
                                    <h6 class="card-subtitle">{{auth()->user()->lawyer_account->branch}}</h6>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body mx-auto">
                              <small class="text-muted text-center">Email address </small>
                                <h6>{{ auth()->user()->email}}</h6> <small class="text-muted text-center p-t-30 db">Phone</small>
                                <h6>{{ auth()->user()->lawyer_account->phone_number}}</h6>
                            </div>
                        </div>
                    </div>
                  {{-- TABLE --}}
                    {{-- <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <h5 class="card-title">Stamp Request Overview</h5>
                                    </div>
                                    <div class="ml-auto">
                                        <a class="btn btn-default">View More</a>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S/N</th>
                                            <th>QUANTITIES</th>
                                            <th>TIME</th>
                                            <th>PRICE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><span class="text-info">3 packs</span> </td>
                                            <td class="txt-oflo">24,Sept 2018, 12:01 am</td>
                                            <td><span class="text-success">#24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2</td>
                                            <td><span class="text-info">3 packs</span> </td>
                                            <td class="txt-oflo">24,Sept 2018, 12:01 am</td>
                                            <td><span class="text-success">#24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">3</td>

                                            <td><span class="text-info">3 packs</span> </td>
                                            <td class="txt-oflo">24,Sept 2018, 12:01 am</td>
                                            <td><span class="text-success">#24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">4</td>

                                            <td><span class="text-info">3 packs</span> </td>
                                            <td class="txt-oflo">24,Sept 2018, 12:01 am</td>
                                            <td><span class="text-success">#24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">5</td>

                                            <td><span class="text-info">3 packs</span> </td>
                                            <td class="txt-oflo">24,Sept 2018, 12:01 am</td>
                                            <td><span class="text-success">#24</span></td>
                                        </tr>

                                        <tr>
                                            <td class="text-center">6</td>

                                            <td><span class="text-info">3 packs</span> </td>
                                            <td class="txt-oflo">24,Sept 2018, 12:01 am</td>
                                            <td><span class="text-success">#24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">7</td>

                                            <td><span class="text-info">3 packs</span> </td>
                                            <td class="txt-oflo">24,Sept 2018, 12:01 am</td>

                                            <td><span class="text-success">#24</span></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> --}}
                </div>
@endsection

@push('scripts')
<!--morris JavaScript -->
    <script src="{{ asset('assets/node_modules/raphael/raphael-min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/morrisjs/morris.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- Popup message jquery -->
    <script src="{{ asset('assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{ asset('dist/js/dashboard1.js')}}"></script>
    <script src="{{ asset('assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
@endpush
