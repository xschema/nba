@extends('layouts.backend.main')
@section('title','View Registration')
@prepend('styles')
<link href="{{asset('assets/node_modules/footable/css/footable.core.css')}}" rel="stylesheet">
    <link href="{{asset('assets/node_modules/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <!-- Page CSS -->
    <link href="{{asset('dist/css/pages/contact-app-page.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/pages/footable-page.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <!-- Custom CSS -->
    <style type="text/css">
        .stat_num{
            font-size: 16px;
            font-weight: 700px;
           padding-left: 20px;
        }
    </style>
@endprepend

@section('content')         

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Lawyers Lists</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('backend.index')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Lawyers Lists</li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- .left-right-aside-column-->
            <div class="c">
                <!-- .left-aside-column-->
                <div class="col-md-12">

                    <ul class="list-style-none" style="margin-top: 30px;">
                        <li>Total Lawyers <span class="stat_num"> {{$lawyer_toatal}}</span></li>
                        <li class="divider"></li>
                                                 
                         <li> Public <span class="stat_num"> {{$public_total}}</span></li>
                        <li> Private <span class="stat_num"> {{$private_total}}</span></li>
                        
                    </ul>
                </div>
                <!-- /.left-aside-column-->
                <div class="col-md-12" style="margin-bottom:30px;">
                    
                    
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>SCN</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Phone Number At</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                    </table>

                    
                    <!-- .left-aside-column-->
                </div>
                <!-- /.left-right-aside-column-->
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{ asset('js/script.js')}}"></script>
  <!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
   -->
     <script src="{{ asset('assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
     
    <script>
     $('.Public').addClass('label-danger') ; 
     $('.Private').addClass('label-success');

         $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('getlawyer.data') !!}',
        columns: [
            {data: 'id', name: 'users.id'},
            {data: 'scn', name: 'lawyer_account.scn'},
            {data: 'fullName', name: 'fullName'},
            {data: 'email', name: 'users.email'},
            {data: 'phone_number', name: 'lawyer_account.phone_number'},
            /*{data: 'action', name: 'action', orderable: false, searchable: false}
            {data: 'id'},
            {data: 'lawyer_account'},
            {data: 'email'},
            {data: 'fullName'},
            {data: 'updated_at'},
            {data: 'action',orderable: false, searchable: false}*/
        ]
    });

    </script>
@endpush
