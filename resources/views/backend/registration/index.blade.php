@extends('layouts.backend.main')
@section('title','View Registration')
@prepend('styles')
<link href="{{asset('assets/node_modules/footable/css/footable.core.css')}}" rel="stylesheet">
    <link href="{{asset('assets/node_modules/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <!-- Page CSS -->
    <link href="{{asset('dist/css/pages/contact-app-page.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/pages/footable-page.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
@endprepend

@section('content')         

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Admin Dashboard</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('backend.index')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Registration</li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- .left-right-aside-column-->
            <div class="contact-page-aside">
                <!-- .left-aside-column-->
                <div class="left-aside bg-light-part">
                    <ul class="list-style-none">
                        <li class="box-label"><a class="ajaxLink" href="{{route('backend-registration.status','all')}}">All Registration <span>{{$total}}</span></a></li>
                        <li class="divider"></li>
                         <li><a class="ajaxLink" href="{{route('backend-registration.status','pending')}}">Pending <span>{{$pending}}</span></a></li>
                                                 
                         <li><a class="ajaxLink" href="{{route('backend-registration.status','public')}}">Public <span>{{$public}}</span></a></li>
                        <li><a class="ajaxLink" href="{{route('backend-registration.status','private')}}">Private <span>{{$private}}</span></a></li>
                        <li><a class="ajaxLink" href="{{route('backend-registration.status','approved')}}" style="color:green;">Approved <span>{{$approved}}</span></a></li>
                        
                    </ul>
                </div>
                <!-- /.left-aside-column-->
                <div class="right-aside " style="margin-bottom:30px;">
                    <div class="right-page-header">
                        <div class="d-flex">
                            <div class="align-self-center">
                                <h4 class="card-title m-t-10">Lawyers Registration List </h4></div>
                            <div class="ml-auto">
                                <input type="text" id="demo-input-search2" placeholder="search contacts" class="form-control"> </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        
                        <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                            <thead>
                                <tr>
                                    <th>SCN</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Practice Type</th>
                                    {{-- <th>Action</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @if (isset($registrations))
                                    @foreach ($registrations as $list)
                            <tr class="ajaxLink" href="{{route('backend-registration.show',$list->id)}}" style="cursor: pointer;">
                                        <td>{{$list->scn}}</td>
                                        <td>
                                            {{$list->salutation.' '.$list->name}}
                                        </td>
                                        <td>{{$list->email}}</td>
                                        <td>{{$list->phone_number}}</td>
                                        <td><span class="label {{$list->practice_type}}">{{$list->practice_type}}</span> </td>
                                        
                                    </tr>
                                    @endforeach
                                @endif                                                       
                                
                            </tbody>
                                                        
                        </table>
                        {{$registrations->links()}}
                    </div>
                    <!-- .left-aside-column-->
                </div>
                <!-- /.left-right-aside-column-->
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{ asset('js/script.js')}}"></script>

    <script>
     $('.Public').addClass('label-danger') ; 
     $('.Private').addClass('label-success');

    </script>
@endpush
