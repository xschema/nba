@extends('layouts.backend.main')
@section('title','Dashboard')
@prepend('styles')
    <link href="{{asset('dist/css/pages/contact-app-page.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">

    <style>
    .text-muted{
        font-size: 1em;
    }
    </style>
    <!-- Custom CSS -->
@endprepend

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Lawyer's Details</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('backend-registration')}}">Home</a></li>
                    <li class="breadcrumb-item active">Details</li>
                </ol>
                @if ($lawyer->status =='Pending')
                            
                    <button type="button" id="approve" href="{{route('backend-registration.approve',['id'=>$lawyer->id,'action'=>'approve'])}}" class="btn btn-success d-none d-lg-block m-l-15 action"><i class="fa fa-plus-circle"></i> Approve</button>
                    <button type="button" class="btn btn-danger d-none d-lg-block m-l-15 reject-btn"><i class="mdi mdi-close-circle"></i> Reject</button>     
                @endif
                            
            </div>
        </div>
    </div>

    <!-- Row -->
    <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
               
                <div class="card">
                    <div class="card-body"> 
                        
                        <small class="text-muted">Name (Sal. First, Middle, Last) </small>
                        <h4 class="card-title">{{$lawyer->salutation.'. '.$lawyer->name}}</h4>
                        <hr>
                        
                        <small class="text-muted">Gender </small>
                        <h5>{{$lawyer->gender}}</h5> 
                        <hr>
                        <small class="text-muted">SCN </small>
                        <h5>{{$lawyer->scn}}</h5> 
                        <hr>

                        <small class="text-muted">Email Address </small>
                        <h5>{{$lawyer->email}}</h5> 
                        <hr>


                        <small class="text-muted p-t-30 db">Personal Phone Number</small>
                        <h5>{{$lawyer->phone_number}}</h5>
                        <hr>

                        <small class="text-muted p-t-30 db">Other Phone Number</small>
                        <h5>{{$lawyer->other_number}}</h5> 
                        <hr>
                    
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Date of Birth</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->dob}}</p>
                                                                                                
                                                {{-- "status" => "Pending"
                                                "created_at" => "2018-12-03 11:02:51" --}}
                                        
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Call to bar</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->year}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Practice Type</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->practice_type}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Branch</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->branch}}</p>
                                    </div>
                                   
                                    <div class="col-md-12">
                                            <hr>
                                    </div>

                                    <div class="col-md-4 col-xs-6"> <strong>Permanent Address</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->permanent_address}}</p>
                                    </div>

                                    <div class="col-md-4 col-xs-6"> <strong>Office Address</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->office_address}}</p>
                                    </div>

                                    <div class="col-md-2 col-xs-6"> <strong>Card Type</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->card_type}}</p>
                                    </div>

                                    <div class="col-md-2 col-xs-6"> <strong>Card Number</strong>
                                        <br>
                                        <p class="text-muted">{{$lawyer->card_number}}</p>
                                    </div>

                                    <div class="col-md-12">
                                            <hr>
                                    </div>
                                    
                                    <div class="col-md-6" style="margin-bottom:20px;">
                                        <img src="{{asset($lawyer->card_image)}}" class="img-modal" height="150" alt="{{$lawyer->card_type}}">                        
                                    </div> 
                                    <hr>
                                    <div class="col-md-12" id="comment-form" style="display:none">
                                    <form action="{{route('backend-registration.reject')}}" class="ajaxForm-reload" id="reject-form" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <textarea name="comment" id="comment" class="form-control" placeholder="Comment" required="required"></textarea>
                                            </div>
                                            <input type="hidden" name="id" value="{{$lawyer->id}}">
                                            
                                           <label class="btn btn-primary active">
                                                <input type="checkbox" name="edit_link" checked autocomplete="off"> Send edit link to user
                                            </label>

                                           <div class="" style="float:right"> 
                                                <button type="button" class="btn btn-default reject-btn">Cancle</button>
                                                <button type="submit" class="btn btn-danger">Send</button>
                                            </div>
                                        </form>
                                        
                                    </div>

                                    <div class="col-md-12">
                                            <hr>
                                    </div>
                                    @if ($lawyer->status =='Pending')
                                    <hr>
                                    <button type="button" id="approve" href="{{route('backend-registration.approve',['id'=>$lawyer->id,'action'=>'approve'])}}" class="btn btn-success m-l-15 action"><i class="fa fa-plus-circle"></i> Approve</button>
                                    <button type="button" class="btn btn-danger m-l-15 reject-btn"><i class="mdi mdi-close-circle"></i> Reject</button>     
                                @endif

                                </div>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->

        <div id="myModal" class="modal">

        <!-- The Close Button -->
        <span class="close-modal">&times;</span>
        
        <!-- Modal Content (The Image) -->
        <img class="modal-content" id="img01">
        
        <!-- Modal Caption (Image Text) -->
        <div id="caption"></div>
        </div>
@endsection

@push('scripts')

<script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>

<script src="{{ asset('js/script.js')}}"></script>

<script>
    
    $('.reject-btn').on('click',function(e){
        
        $('#comment-form').toggle('slow');
        $('#comment').val('');

    });

    $(".action").click(function(e){
            e.preventDefault();
            $action = $(this).attr('href');
            $action_name = $(this).attr('id');
            
            $message = "Please confirm all Information provided are correct";
            //$("#error-message").html('');
            swal({
                    title: "Are you sure approve?",
                    text: $message,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, "+$action_name,
                    closeOnConfirm: true
                }, function(isConfirm){
                    
                    if(isConfirm){
                        //alert($action);
                        setTimeout(function(){
                            loadingModal('loading');
                        },10);
                        
                        
                        $.ajax({
                        url : $action,
                        type: 'GET',
                        success: function(data){

                            if(data.success){
                                setTimeout(function(){
                                    alert(data.success);
                                    window.location.reload();
                                },1500)
                                
                            }else{

                                alert(data.error);
                            }
                        },
                        error: function(){

                            loadingModal('close');
                            alert('Error Occured, Please try again later !!!');
                            //displayError('Error Occured, Please try again later !!!');
                        }

                        });
                    }
                    
                });


                //
        });

        /*function loadingModal(action) {
        if(action=="loading"){
            swal({
                title: "Loading...",
                text: "Please be patient while loading your request.",
                showConfirmButton: false
            });
        }
        else{
            swal.close();
        }
    }*/

</script>
    
@endpush