@extends('layouts.backend.main')
@section('title','Dashboard')
@prepend('styles')
<!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Dashboard 1 Page CSS -->
<link href="dist/css/pages/dashboard1.css" rel="stylesheet">
<style type="text/css">
  .badge-default {
    background-color: #737980;
    color: white;
}
</style>
@endprepend

@section('content')
<!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Admin Dashboard</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->



                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3> 
                                            <p class="text-muted">Total Lawyer</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary">{{  $total_lawyers }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3>
                                            <p class="text-muted">Stamp Request</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary">{{  $total_requests_count }}</h2>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-cyan" role="progressbar" style="width:%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text-outline"></i></h3>
                                            <p class="text-muted"> New Tickets </p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-danger">{{$pending_tickets}}</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: {{$pending_ticket_percentage}}%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text"></i></h3>
                                            <p class="text-mute">Total Tickets</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-success">{{$total_tickets}}</h2>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- Comment - table -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- Comment widgets -->
                    <!-- ============================================================== -->
                    @if (auth()->user()->role == 'manager')
                      <div class="col-lg-5">
                    @else
                      <div class="col-lg-12">
                    @endif

                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Recent Tickets</h5>
                            </div>
                            <!-- ============================================================== -->
                            <!-- Comment widgets -->
                            <!-- ============================================================== -->
                            <div class="comment-widgets">
                               <!-- Comment Row -->
                               @if(count($tickets)>0)
                                @foreach($tickets as $ticket)
                               <a href="{{route('backend.ticket.show',[ "t_num" => $ticket->ticket_number, 'id' =>$ticket->id])}}">

                                <div class="d-flex no-block comment-row">
                                    <div class="p-2"><span class="round"><img src="{{$ticket->user->avatar_url}}" alt="{{$ticket->user->name}}"                     width="50"></span></div>
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium">{{$ticket->user->name}}</h5>
                                        <p class="m-b-10 text-muted">{{str_limit($ticket->message, 80,'...')}}</p>
                                        <div class="comment-footer">
                                            <span class="text-muted pull-right">{{$ticket->updated_at->diffForHumans()}}</span> <span class="badge badge-pill badge-{{$ticket->status}}">{{$ticket->status}}</span>
                                        </div>
                                    </div>
                                </div>
                                  </a>
                                @endforeach
                                @else
                                <p class="text-center">No Ticket found</p>
                                 @endif



                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Table -->
                    <!-- ============================================================== -->
                    <div class="col-lg-7">
                        <div class="card">
                            @if(Auth()->user()->role =='manager')
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <h5 class="card-title">Stamp Request Overview</h5>
                                        <h6 class="card-subtitle">Recent Purchases</h6>
                                    </div>
                                    @if (count($todays_requests))
                                      <div class="ml-auto">
                                        <a id="export-today-button" class="btn btn-success">Export</a>
                                      </div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body bg-light">
                                <div class="row">
                                    <div class="col-6">
                                        <h3>Today {{date('d,M, Y')}} </h3>
                                        <h5 class="font-light m-t-0">Report for today</h5>
                                    </div>
                                    <div class="col-6 align-self-center display-6 text-right">
                                        <h2 class="text-success">{{naira($total_sales)}}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S/N</th>
                                            <th>NAME</th>
                                            <th>QUANTITIES</th>
                                            <th>DATE</th>
                                            <th>AMOUNT</th>
                                            {{-- <th>CHARGES</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($todays_requests as $request)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td class="txt-oflo">{{$request->buyer->name}}</td>
                                            <td>
                                              <span >
                                                {{$request->details}}
                                              </span>
                                            </td>
                                            <td > {{ $request->created_at->diffForHumans() }}</td>
                                            <td><span class="text-success"> {{ naira($request->amount/100) }}</span></td>
                                            {{-- <td><span class="text-success"> {{ naira($request->charges/100) }}</span></td> --}}
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                                @unless ( count($todays_requests) )
                                  <h3 class="text-center  my-5">There are no paid requests today</h3>
                                @endunless
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
@endsection

@push('scripts')
    <script type="text/javascript">
      $('.badge-New').addClass('badge-success');
      $('.badge-Pending').addClass('badge-danger');
      $('.badge-Answered').addClass('badge-info');
      $('.badge-Closed').addClass('badge-default');

      $('#export-today-button').on('click', function(){

        $el =  $(this)

        $el.text('Processing...')

        $.get('exports/today')
          .then(data=>{
            $el.text('Export')
          })
          .catch(err => {
            console.log(err)
          })
      })

    </script>
@endpush
