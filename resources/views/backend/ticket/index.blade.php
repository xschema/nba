@extends('layouts.backend.main')
@section('title','Dashboard')
@push('styles')

<link href="{{ asset('dist/css/pages/inbox.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<style type="text/css">
  /* .list-group-item: hover{
    background-color: black;
  } */
    .list-group .badge{
        float:right;
    }
    .pagination{
        float: right;
    }
    .label-default {
    background-color: #737980;
}
.list-group-item{
    cursor: pointer;
}
</style>
@endpush

@section('content')

 <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Tickets</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="">Home</a></li>
                <li class="breadcrumb-item active">Tickets</li>
            </ol>

        </div>
    </div>
</div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="row">
                                 <div class="col-lg-3 col-md-4">
                    <div class="card-body inbox-panel">
                        <a href="{{route('backend.ticket.index')}}" class="btn btn-success m-b-20 p-10 btn-block waves-effect waves-light">Tickets</a>
                        <ul class="list-group list-group-full">
                            <li class="list-group-item ajaxLink" href="{{route('backend.ticket.index')}}">
                                <i class="mdi mdi-gmail"></i> Total<span class="badge badge-success ml-auto">{{$total}}</span>

                            </li>
                             <li class="list-group-item ajaxLink" href="{{route('backend.ticket.filter','Pending')}}"> 
                               
                                    <i class="mdi mdi-gmail"></i> Pending
                              <span class="badge badge-danger ml-auto">{{$pending}}</span>

                             </li>
                             <li class="list-group-item ajaxLink" href="{{route('backend.ticket.filter','Answered')}}"> 
                                <i class="mdi mdi-gmail"></i> Answered <span class="badge badge-info ml-auto">{{$answered}}</span>
                                
                             </li>
                             <li class="list-group-item ajaxLink" href="{{route('backend.ticket.filter','Closed')}}"> 
                                <i class="mdi mdi-gmail"></i> Closed
                                <span class="badge badge-default ml-auto">{{$closed}}</span>
                            
                             </li>
                        </ul>
                        
                    </div>
                </div>

                  <div class="col-lg-9 col-md-8 bg-light border-left">
                     @if(count($tickets)>0)
                    <div class="card-body" style="height: 105px">
                            <h3> Tickets</h3>
                            {{$tickets->links()}}
                    </div>

                    <div class="card-body p-t-5 show-content">
                        <div class="card b-all shadow-none">
                            <div class="inbox-center table-responsive">
                                <table class="table table-hover no-wrap">
                                    <tbody>
                                            @foreach($tickets as $ticket)
                                                @php
                                                 $status = ($ticket->status =='New') ? 'Pending' : $ticket->status;
                                                $subject = (strlen($ticket->subject)> 40)? substr($ticket->subject, 0,40)."..." : $ticket->subject;
                                                $date = $ticket->updated_at->diffForHumans();
                                                $attachment = '';
                                                //check if has attachment
                                                if(count($ticket->attachment)< 1 )
                                                    $attachment = 'hide';
                                                @endphp
                                                <tr class="{{$status}}} ajaxLink" href="{{route('backend.ticket.show',[ "t_num" => $ticket->ticket_number, 'id' =>$ticket->id])}}" style="cursor: pointer;">
                                                   <td class="hidden-xs-down">{{$ticket->user->name}}</td>
                                                    <td class="max-texts">
                                                        
                                                             {{$subject}}
                                                        
                                                    </td>
                                                    <td>
                                                        <span class="label label-{{$status}}">{{$status}}</span>
                                                             <i class="fa fa-paperclip {{$attachment}}"></i>
                                                    </td>
                                                    <td class="text-right"> <small>{{$date}}</small> </td>
                                                </tr>

                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="card-body show-content">
                        <p style="padding-top: 20px; text-align: center;">No Ticket Found</p>
                    </div>
                    @endif
                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->

@endsection

@push('scripts')
<!--morris JavaScript -->
    <script src="{{ asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/script.js')}}"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        $('.label-New').addClass('label-success');
            $('.label-Pending').addClass('label-danger');
            $('.label-Answered').addClass('label-info');
            $('.label-Closed').addClass('label-default');

        
      });
    </script>
@endpush
