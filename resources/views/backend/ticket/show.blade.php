@extends('layouts.backend.main')
@section('title','Ticket')
@push('styles')

<link href="{{ asset('dist/css/pages/inbox.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">

<style type="text/css">
  /* .list-group-item: hover{
    background-color: black;
  } */
    .list-group .badge{
        float:right;
    }
    .pagination{
        float: right;
    }
    .label-default {
    background-color: #737980;
}
.list-group-item{
    cursor: pointer;
}
</style>
@endpush

@section('content')

 <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Tickets</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('backend.ticket.index')}}">Ticket</a></li>
                  <li class="breadcrumb-item active">View Ticket</li>
              </ol>

          </div>
      </div>
  </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row pt-2 pl-2">
                    <div class="col-sm-12" id="flash-message">
                        @if($flash = session('alert'))
                            @include('partials.alert')
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="row">
                                 <div class="col-lg-3 col-md-4">
                                  <div class="card-body inbox-panel">
                                      <a href="{{route('backend.ticket.index')}}" class="btn btn-success m-b-20 p-10 btn-block waves-effect waves-light">Tickets</a>
                                      <ul class="list-group list-group-full">
                                          <li class="list-group-item ajaxLink" href="{{route('backend.ticket.index')}}">
                                            <i class="mdi mdi-gmail"></i> Total<span class="badge badge-success ml-auto">{{$total}}</span></li>
                                           <li class="list-group-item ajaxLink" href="{{route('backend.ticket.filter','Pending')}}"> 
                                            <i class="mdi mdi-gmail"></i> Pending
                                            <span class="badge badge-danger ml-auto">{{$pending}}</span>
                                           </li>
                                           <li class="list-group-item ajaxLink" href="{{route('backend.ticket.filter','Answered')}}"> 
                                            <i class="mdi mdi-gmail"></i> Answered
                                            <span class="badge badge-info ml-auto">{{$answered}}</span>
                                           </li>
                                           <li class="list-group-item ajaxLink" href="{{route('backend.ticket.filter','Closed')}}"> 
                                            <i class="mdi mdi-gmail"></i> Closed
                                              <span class="badge badge-default ml-auto">{{$closed}}</span>
                                           </li>
                                      </ul>
                                  </div>
                                </div>

                  @php
                //change new tickets to Pending

                    $status = ($ticket->status =='New') ? 'Pending' : $ticket->status;
                    $image = $ticket->user->avatar_url;
                @endphp


                <div class="col-lg-9 col-md-8 bg-light border-left">
                    <div class="card b-all shadow-none">
                        <div class="card-body">
                            <h4 class="card-title m-b-0">{{$ticket->subject}} <small>Ticket: #{{$ticket->ticket_number}}</small></h4>
                        </div>
                        <div>
                            <hr class="m-t-0">
                        </div>
                        <div class="card-body">
                            <div class="d-flex m-b-20">
                                <div>
                                    <img src="{{asset($image)}}" alt="user" width="40" class="img-circle" />
                                </div>
                                <div class="p-l-10">
                                    <h4 class="m-b-0">{{$ticket->user->name}} <small> ({{$ticket->user->lawyer_account->scn}})</small></h4>
                                    <small class="text-muted">From: {{$ticket->user->email}}</small> &nbsp;&nbsp; <span class="label label-{{$status}}" id="btn-label">{{$status}}</span>
                                </div>
                            </div>
                            <p>{{$ticket->message}}</p>

                        </div>

                        <div>
                            <hr class="m-t-0">
                        </div>

                        @if(count($ticket->attachment)>0)
                            <div class="card-body">

                                <h4><i class="fa fa-paperclip m-r-10 m-b-5"></i> Attachments <span>({{count($ticket->attachment)}})</span></h4>

                                <div class="row">
                                    @foreach($ticket->attachment as $attachment)
                                    <div class="col-md-2">
                                        <img class="img-thumbnail img-responsive img-modal" alt="attachment" src="{{asset($attachment->path)}}">
                                    </div>
                                    @endforeach
                                </div>

                            </div>

                              <div>
                                <hr class="m-t-0">
                            </div>
                        @endif

                         @if(count($ticket->reply)>0)
                            <div class="card-body" style="background-color: #eceaea33;">
                            <h4>Replies</h4>
                                <div class="profiletimeline">

                                     @foreach($ticket->reply as $reply)
                                         @php
                                         $name = $ticket->user->name;
                                            if($reply->user_id == Auth::user()->id){
                                            $name ='Me';
                                        }
                                         @endphp
                                    <div class="sl-item">
                                        <div class="sl-left">
                                            <img src="{{ asset($image)}}" alt="user" class="img-circle" />
                                        </div>
                                        <div class="sl-right">
                                            <div>
                                                <span class="link">{{$name}}&nbsp; <small>({{$reply->user->role}})</small> </span>&nbsp;&nbsp;&nbsp;&nbsp; <span class="sl-date text-right" > {{$reply->created_at->diffForHumans()}}</span>
                                                <p>{{$reply->message}} </p>
                                                <div class="row">
                                                    @if(count($reply->attachment)>0)
                                                        @foreach($reply->attachment as $attachment)
                                                        <div class="col-md-2">
                                                            <img class="img-thumbnail img-responsive img-modal" alt="attachment" src="{{asset($attachment->path)}}"> 
                                                        </div>
                                                         @endforeach
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        @endif

                            <div>
                                <hr class="m-t-0">
                            </div>
                            @if($ticket->status !='Closed')
                        <div class="card-body" id="reply-col">
                            <button class="btn btn-warning" id="reply-btn"><i class="mdi mdi-reply"></i> Reply</button>
                            <button class="btn btn-danger pull-right" id="close-ticket-btn"><i class="mdi mdi-pencil-off"></i> Close Ticket</button>
                            <div class="hide" id="reply-form">
                            <h4>Reply</h4>
                            <form action="{{route('ticket.reply')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <textarea class="textarea_editor form-control" name="message" rows="5" placeholder="Enter text ..."></textarea>
                                </div>
                                <input type="hidden" name="ticket_number" value="{{$ticket->ticket_number}}">

                                <h4><i class="ti-link"></i> Attachment</h4>
                            <span class="dropzone">
                                <div class="fallback">
                                    <input name="attached[]" type="file" multiple />
                                </div>
                            </span>

                                <button type="submit" class="btn btn-success m-t-10">
                                    Send
                                </button>
                            </form>
                        </div>
                        </div>
                        @endif
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close-modal">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
@endsection

@push('scripts')
<!--morris JavaScript -->
    <script src="{{ asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/script.js')}}"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        setTimeout(function(){
          $('#flash-message').hide('slow')
        }, 3000)
        $('.label-New').addClass('label-success');
            $('.label-Pending').addClass('label-danger');
            $('.label-Answered').addClass('label-info');
            $('.label-Closed').addClass('label-default');
            //reply button toggle
            $('#reply-btn').on('click',function(e){
                $('#reply-form').toggle('slow');
            });
            
            $('#close-ticket-btn').on('click',function(e){
                 swal({
                      title: "Are you sure?",
                      text: 'Please click yes to confirm and No to cancel',
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Close Ticket",
                      closeOnConfirm: false
                  }, function(isConfirm){
                      if (isConfirm) {
                        var action = "{{route('backend.ticket.close',   $ticket->ticket_number)}}";
                        $.ajax({
                            url : action,
                            type: 'post',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                        success : function(data){
                        if(data.success){

                                     swal('Closed!', 'Ticket Closed successfully',  'success' )
                                     $('#reply-col').hide('slow');

                                     $('#btn-label').addClass('label-default');
                                     $('#btn-label').text('Closed');
                        }else{
                                swal('Error!', 'Error occoured, Please try again later',  'error' ) ;
                        }
        },
        error: function(){
           swal('Error!', 'Error occoured, Please try again later',  'error' ) ;
        }
        });
                  }
              });
            });
      });
    </script>
@endpush
