@extends('layouts.backend.main')
@section('title','Dashboard')
@prepend('styles')
@php
    $role = auth()->user()->role
@endphp

<script>
  window.userRole = `{!! $role !!}`
</script>
<!-- Dashboard 1 Page CSS -->
@endprepend

@section('content')
<!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Admin Dashboard</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div>
                            <h5 class="card-title">Stamp Request</h5>
                            <h6 class="card-subtitle">All Purchases</h6>
                        </div>
                        <div class="ml-auto">
                        </div>
                    </div>
                </div>
                <div id="app">
                    <flash></flash>
                    <stamp-requests-list> </stamp-requests-list>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<!--morris JavaScript -->
    <!-- Popup message jquery -->
    <!-- Chart JS -->
      <script src="{{asset('js/manifest.js')}}"></script>
    <script src="{{asset('js/vendor.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
@endpush
