@extends('layouts.frontend.verification')
@section('title','Edit-Record')
@push('styles')
<link href="{{asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/node_modules/dropify/dist/css/dropify.min.css')}}">

<style>
label{
    float: left;
}
#msform input{
    font-size: 15px;
}

#msform input{
    padding: 10px;
}
#eliteregister li{
    width: 24.33%;
}
#wrapper {
    /* width: 100%; */
    overflow-x: scroll;
    }
    .result{
        font-weight: bold;
        text-transform: uppercase;
        padding-left: 5px;

    }
    hr{
        margin-top: 0rem;
    }
    .prev-row{
        width: 100%;
        max-height: 100px;
        display: block;
        background-color: #d2c8c833;
        padding-left: 5px;
    }
</style>
@endpush

@section('content')
 
<div class="register-box">
   {{-- <div class="col-sm-12">
        <a href="#" style="color: #326936"> <i class="fa fa-long-arrow-left"></i> Return Back </a>
    </div> --}}

    <div class="">
        <a href="{{route('register')}}" class="text-center"><img src="{{asset('img/logo.png')}}" alt="NBA" width="80" /></a>
        <a href="{{ route('verification.index')}}" style="color: #326936"> <i class="fa fa-long-arrow-left"></i> Return Back </a>
        <h3 class="text-center" style="">Lawyer's Information Confirmation</h3>
        <!-- multistep form -->
    <form id="msform" method="POST" class="" action="{{route('registration.update', $registration->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <!-- progressbar -->
            <ul id="eliteregister">
                <li class="active">Step 1</li>
                <li>Step 2 </li>
                <li>Step 3</li>
                <li>Step 4</li>
            </ul>

            <div class="row pt-2 pl-2">
                <div class="col-sm-12" id="flash-message">
                    @if($flash = session('alert'))
                        @include('partials.alert')
                    @endif
                </div>
            </div>
            
                <div class="card">
                    <div class="card-body">
                    <p style="color: #f00c;">{{$comment->comment}}</p>
                    </div>
                </div>
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Step 1</h2>

                <div class="row">
                    <div class="col-md-6">
                        <label for="salutation">Salutation</label>

                    <select class="custom-select form-control" id="salutation" name="salutation" style="margin-bottom: 18px" required>

                        <option value="Barr" {{ $registration->salutation == 'Barr' ? 'selected="selected"' : '' }} >Barr</option>
                        <option value="SAN" {{ $registration->salutation == 'SAN' ? 'selected="selected"' : '' }}>SAN</option>
                    </select>

                    </div>
                    <div class="col-md-6">
                        <label>First Name</label>
                        <input type="text" name="first_name" value="{{$registration->first_name}}" placeholder="First Name" required />
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <label for="middle Name">Middle Name</label>
                        <input type="text" name="middle_name" value="{{$registration->middle_name}}" placeholder="Middle Name" />
                    </div>
                    <div class="col-md-6">
                        <label for="Surname Name">Last Name</label>
                <input type="text" name="last_name" value="{{$registration->last_name}}" placeholder="Surname" required />
                    </div>
                </div>          

                <div class="row">
                    <div class="col-md-6">
                        <label for="gender">Gender</label>

                        <select class="custom-select form-control" id="gender" name="gender" style="margin-bottom: 18px" required>
                        <option value="">Choose Category</option>

                        <option value="Female" {{ $registration->gender == 'Female' ? 'selected="selected"' : '' }}> Female</option> 
                        <option value="Male"{{ $registration->gender== 'Male' ? 'selected="selected"' : '' }}>Male</option>
                    </select>
                    </div>

                    <div class="col-md-6">
                        <label for="phone number">Date of Birth</label>
                        <input type="text" name="dob" value="{{$registration->dob}}" class="form-control mydatepicker" placeholder="dd/mm/yyyy">
                    </div>

                </div>
               

                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>

            <fieldset>

                <h2 class="fs-title">Step 2</h2>

                <div class="row">

                    <div class="col-md-6">
                        <label for="phone number">Business Phone Number(required)</label>
                        <input type="text" name="phone_number" value="{{$registration->phone_number}}" placeholder="Phone Number(Required)" required />

                    </div>

                    <div class="col-md-6">
                        <label for="phone number">Personal Phone Number(Not required)</label>
                <input type="text" name="other_number" value="{{$registration->other_number}}" placeholder="Phone Number" required /> 
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-6">
                      <label for="Email">Email</label>
                        <input type="email" name="email" value="{{$registration->email}}" placeholder="Email" required />  
                    </div>

                    <div class="col-md-6">
                        <label for="SCN">Enrollment Number</label>
                <input type="text" name="scn" value="{{$registration->scn}}" placeholder="SCN123456AB" required />
                    </div>
                </div>     

                <div class="row">
                    <div class="col-md-6">
                        <label for="phone number">Enrollment Year(Call to bar)</label>
                        <input type="text" name="year" value="{{$registration->year}}" class="form-control mydatepicker" placeholder="dd/mm/yyyy">
                    </div>

                    <div class="col-md-6">
                        <label for="Branch">Branch</label>
                        <input type="text" name="branch" value="{{$registration->branch}}" placeholder="Branch" required />
                    </div>
                </div>

               
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>           
            
            <fieldset>
                <h2 class="fs-title">Step 3</h2>



                <label for="Practice Category">Practice Type</label>
                <select class="custom-select form-control" id="practice_type" name="practice_type" style="margin-bottom: 18px" required>
                    <option>Choose Category</option>
                    <option value="Private" {{ $registration->practice_type == 'Private' ? 'selected="selected"' : '' }}>Private</option>
                    <option value="Public" {{ $registration->practice_type == 'Public' ? 'selected="selected"' : '' }}>Public</option>
                </select>
                <br />

                <label for="Branch">Permanent Address</label>
                <textarea name="permanent_address" required rows="1">{{$registration->permanent_address}}</textarea>


                <label for="Branch">Office Address</label>
                <textarea name="office_address" required rows="1">{{$registration->office_address}}</textarea>

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>

            <fieldset>
                    <h2 class="fs-title">Step 4</h2>

                <div class="row">
                    <div class="col-md-6">
                        <label for="Practice Category">Means of Identification </label>

                        <select class="custom-select form-control" id="card_type" name="card_type" style="margin-bottom: 18px" required>
                            <option>Choose Category</option>
                            <option value="NBA Affinity Card" {{ $registration->card_type== 'NBA Affinity Card' ? 'selected="selected"' : '' }}>NBA Affinity Card</option>
                            <option value="National Id" {{ $registration->card_type== 'National Id' ? 'selected="selected"' : '' }}>National Id</option>
                            <option value="Voter Card" {{ $registration->card_type== 'Voter Card' ? 'selected="selected"' : '' }}>Voter's Card</option>
                            <option value="Driver's License" {{ $registration->card_type== 'Driver\'s License' ? 'selected="selected"' : '' }}>Driver's License</option>
                            <option value="International Passport" {{ $registration->card_type== 'International Passport' ? 'selected="selected"' : '' }}>International Passport</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                      <label for="serial number">Card Number</label>
                    <input type="text" name="card_number" value="{{$registration->card_number}}" placeholder="Card Number" required />
  
                    </div>
                </div>
                
                    <!-- <div class="row">
                        
                        <label for="input-file-max-fs">Upload (5M max)</label>
                        <input type="file" value="" id="input-file-max-fs" class="dropify" name="card_image" data-max-file-size="5M" />
                                      
                    </div> -->
                    <input type="hidden" name="hash" value="{{$comment->hash}}">
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" id="preview" class="next action-button" value="Preview" />
                </fieldset>

                <fieldset style="background-color: rgba(204, 204, 204, 0.2)">
                    <h2 class="fs-title">Confirmation page</h2>
                    <div class="row" id="final-step">
                        <div class="col-md-6" style="text-align:left;">
                            <span class="prev-row">
                            Salutation : <span id="prev_salutation" class="result"></span>  <hr>
                            </span>
                             <span class="prev-row">
                            First name : <span id="prev_first_name" class="result"></span>  <hr>
                        </span>
                         <span class="prev-row">
                            Middle Name : <span id="prev_middle_name" class="result"></span> <hr>
                            </span>
                             <span class="prev-row">
                            Last Name: <span id="prev_last_name" class="result"></span> <hr>
                            </span>
                             <span class="prev-row">
                            DOB: <span id="prev_dob" class="result"></span><hr>
                        </span>
                         <span class="prev-row">
                            Email: <span id="prev_email" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Business Phone Number: <span id="prev_phone_number" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Personal Phone Number: <span id="prev_other_number" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Gender: <span id="prev_gender" class="result"></span> <hr>
                        </span>
                         
                        </div>

                        <div class="col-md-6" style="text-align:left;">
                            <span class="prev-row">
                            Enrolment Number: <span id="prev_scn" class="result"></span> <hr>
                        </span>
                             <span class="prev-row">
                            Year: <span id="prev_year" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Practice Type: <span id="prev_practice_type" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Branch: <span id="prev_branch" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Permanent Address: <span id="prev_permanent_address" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Office Address: <span id="prev_office_address" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Card Type: <span id="prev_card_type" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Card Number: <span id="prev_card_number" class="result"></span> <hr>
                        </span>
                            {{-- Card_image: <span id="prev_card_image" class="result"></span> --}}

                        </div>

                    </div>
                                       

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" name="submit" id="" class="action-button" value="Submit" />

                </fieldset>

                
        
        </form>

        <div class="clear"></div>
       {{--  <p> <a href="{{route('verification.index')}}">Home</a></p> --}}
    </div>
</div>

@endsection

@push('scripts')
<script src="{{asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/node_modules/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
    <script>
        $(document).ready(function() {
            //jQuery('.mydatepicker, #datepicker').datepicker();
            jQuery('.mydatepicker').datepicker({
        autoclose: true,
        todayHighlight: true
    });
            $('.dropify').dropify();

            $('#preview').on('click',function(){
                var myForm =  document.getElementById('msform');
                
                var formData = new FormData(myForm);

                //const finalStep = document.querySelector('#final-step') 

                for(const [key, value] of formData){
                    //if()
                    $('#prev_'+key).html(value);
                // else
                //     $('#prev_'+key).html('cannot be empty');
                }
                
            });

            // $('#practice_type').on('click',function()){
            //     var myForm =  document.getElementById('msform');
            //     var formData = new FormData(myForm);
            // }
        });
    </script>
@endpush
