@extends('layouts.frontend.verification')
@section('title','Verification')
@push('styles')
    <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <style type="text/css">
        #msform input{
            font-size: 15px;
        }
        #wrapper {
        /* width: 100%; */
        overflow-x: scroll;
        }
    </style>
@endpush
@section('content')
<div class="register-box">
    <div class="">
        <a href="javascript:void(0)" class="text-center m-b-20"><img src="{{asset('img/logo.png')}}" alt="NBA" width="80" /></a>
        <!-- multistep form -->
        <h3 class="text-center" style="">Stamp System Account Setup</h3>
        <form id="msform" action="" method="post">
            
            <div class="text-center text-danger hide" id="error-message"> </div>
            <div class="text-center text-success hide" id="success-message"> </div>

            <fieldset>
                <h2 class="fs-title">Signup Details</h2>
                <h3 class="fs-subtitle">Provide your sign up details</h3>

                
                    <input type="email" id="email" name="email" placeholder="Email" />
                    <select class="custom-select form-control" id="category" name="category" style="margin-bottom: 18px">
                    <option>Choose Category</option>
                        <option value="Private">Private</option>
                        <option value="Public">Public</option>
                    </select>
                    <br />
                <input type="text" name="phone_number" id="phone_number" placeholder="Phone Number" />
                <input type="password" name="password" id="password" placeholder="password" />
                <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" />
                <input type="hidden" name="role" id="role" value="Lawyer">
                <!-- <input type="button" name="previous" class="previous action-button" value="Previous" /> -->
                <button type="submit" name="submit" id="signup-btn" class="submit action-button"> Submit</button>
            </fieldset>
        </form>
        <div class="clear"></div>

    </div>
</div>
@endsection

        @push('scripts')
    <script src="{{ asset('assets/node_modules/sweetalert/sweetalert.min.js')}}"></script>

        <script type="text/javascript">

        $(".submit").click(function(e){
            e.preventDefault();
             //check if password are correct
            //var year = $('#call_to_bar').val();category
            var email = $('#email').val();
            var role = $('#role').val();
            var phone_number = $('#phone_number').val();
            var category = $('#category').val();
            var password = $('#password').val();
            var confirm_password = $('#confirm_password').val();
            //console.log(password+"con-pwd"+)
            
            if(password != confirm_password || password == "" || confirm_password == ""){
                $('#password').addClass('has-error');
                $('#confirm-password').addClass('has-error');
                displayError('Passwords does not match');
                return;
            }
            $message = "Please ensure all Information provided are correct before submitting.";
            $("#error-message").html('');
            swal({
                    title: "Are you sure?",
                    text: $message,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Signup",
                    closeOnConfirm: true
                }, function(isConfirm){
                    //swal.close();
                    if (isConfirm) {
                       $('#signup-btn').text('Processing...').attr('disabled', true);
                        loadingModal('loading');
                       var action = "{{route('verification.store')}}";
                  var token = '{{csrf_token()}}';
                  $.ajax({
            url : action,
            type: 'POST',
            data: {'_token': token, 'email': email, 'role': role,'category': category, 'phone_number': phone_number, 'password':password},
            success: function(data){

                if(data.success){
                     $("#error-message").html('');
                     $("#success-message")
                     .html('Sign up Successful, you will be redirected to your dashboard')
                     .show('slow');
                    //displayError(data.success);
                    setTimeout( function () {
                        return window.location ="{{route('frontend.index')}}";
                    }, 1000);
                }

                 if(data.error){
                    displayError(data.error);
                    loadingModal('close');
                     $('#signup-btn').text('Submit').attr('disabled', false);
                }
        },
        error: function(){
            loadingModal('close');
            displayError('Error Occured, Please try again later !!!');
             $('#signup-btn').text('Submit').attr('disabled', false);
        }

            });
                }
                });


                //
        });

        function loadingModal(action) {
        if(action=="loading"){
            swal({
                title: "Loading...",
                text: "Please be patient while loading your request.",
                showConfirmButton: false
            });
        }
        else{
            swal.close();
        }
    }
        //hide this field set
        function hideFiledset(current_fs,next_fs){
            current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50)+"%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'transform': 'scale('+scale+')'});
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
        }

        function displayError(message){
            var container = $("#error-message");
             container.html(message);
             container.show(500);
        }

        </script>
        @endpush
