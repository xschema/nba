@extends('layouts.auth.login')
@section('title','Rest Password')

@section('content') 
     <div class="login-register" style="top: -20px">

                <!-- multistep form -->
        <a href="javascript:void(0)" class="text-center m-b-20" style="display: block"><img src="{{asset('img/logo.png')}}" alt="NBA" width="120" /></a>

        <div class="login-box card">
            <div class="card-body">

                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required> 
                        </div>
                    </div>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Send Password Reset Link</button>
                            <br />
                            <br />
                            <a href="{{route('login')}}" class="btn btn-info">Login</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    
    
@endsection

@push('scripts')


@endpush