<!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                        aria-expanded="false"><img src="{{ auth()->user()->avatar_url }}" alt="user-img" class="img-circle"><span
                            class="hide-menu">{{ auth()->user()->name }}</span></a>
                    <ul aria-expanded="false" class="collapse">
                       {{--  <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li> --}}
                        <li>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                          </form>
                          <a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();" >
                            <i class="fa fa-power-off"></i>
                             Logout
                          </a>
                        </li>
                    </ul>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ route('backend.index') }}"><i class="icon-speedometer"></i>
                        <sspan class="hide-menu">Dashboard</span>
                    </a>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="ti-palette"></i><span class="hide-menu">Stamp Requests</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('all-requests') }}">All Requests</a></li>
<!--                         <li><a href="">Export Pending</a></li>
                     -->
                   </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="ti-email"></i><span class="hide-menu">Messages</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('backend.ticket.index')}}">Complaint</a></li>
                    </ul>
                </li>

                <li> <a class="waves-effect waves-dark" href="{{ route('backend-registration') }}">
                    <i class="fa fa-users"></i>
                        <span class="hide-menu">Registration</span>
                    </a>
                </li>
               {{--  <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="ti-write"></i><span class="hide-menu">Reports </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="">Stamps</a></li>
                        <li><a href="">Payments</a></li>
                        <li><a href="">Deliveries</a></li>
                    </ul>
                </li> --}}
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
