<div class="alert alert-{{$flash['status']}} mt-2 mx-auto" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="la la-close"></span>
    </button>
    <div class="ks-inline-image-block">
        {{ $flash['message'] }} 
    </div>
</div>     