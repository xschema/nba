@php
$name = auth()->user()->first_name." ".auth()->user()->last_name;
@endphp
<div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">

                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                        aria-expanded="false"><img src="{{ auth()->user()->avatar_url }}" alt="user-img" class="img-circle"><span
                            class="hide-menu">{{title_case($name)}}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('frontend.profile')}}"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="{{route('logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{route('frontend.index')}}"><i class="icon-speedometer"></i>
                        <sspan class="hide-menu">Dashboard</span>
                    </a>

                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="ti-palette"></i><span class="hide-menu">Stamp Requests</span></a>
                    <ul aria-expanded="false" class="collapse">
                      @if(!auth()->user()->hasCart())
                        <li><a href="{{route('stamp-requests.create')}}">New Request</a></li>
                      @endif
                        <li><a href="{{route('buyer-requests')}}">My Requests</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="ti-email"></i><span class="hide-menu">Support</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('ticket.create')}}">Register Complaint</a></li>
                        <li><a href="{{route('ticket.index')}}">My Complaints</a></li>
                    </ul>
                </li>




            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
