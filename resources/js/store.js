import Vue from 'vue'

export default new Vue({
  data() {
  return{
    state :{
      numberOfPacks: 0,
      numberOfSheets: 0,
      perPack : 400000,
      perSheet : 150000,
      expiry: '',
      name:'',
      scn:'',
      address : '',
    }
  }
},
computed : {
    convenienceCharge() {
      return 0.03 *  this.totalCost
    },

    totalCost () {
      const packsCost = parseInt(this.state.numberOfPacks * this.state.perPack)
      const sheetsCost = parseInt(this.state.numberOfSheets * this.state.perSheet)
      return packsCost + sheetsCost
    },
    totalCharge(){
      return this.totalCost + this.convenienceCharge
    },
    totalCostFormatted() {
      const naira = parseFloat( this.totalCharge / 100 ).toLocaleString('en');
      return `₦ ${(naira)}.00`
    },
    convenienceChargeFormatted() {
      const naira = parseFloat( this.convenienceCharge / 100 ).toLocaleString('en');
      return `₦ ${(naira)}.00`
    },
    pickUpAddress() {
      return this.state.address === '' ? `${this.state.buyer.branch} Branch Office` : this.state.address
    },
    stampType(){
      return this.state.buyer.category
    },
    expiryChoices(){
      const today = new Date()
      const choices = [
          {type:'new', expiry: '31st March, 2020'}
      ]

      if(today.getMonth() < 3 ) choices.push({type:'old', expiry: '31st March, 2019'})

      return choices
    }

}
})
