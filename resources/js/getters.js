export default {
  stampType : state => state.stampType,
  numberOfPacks : state => state.numberOfPacks,
  numberOfSheets : state => state.numberOfSheets,
  totalCost : state => state.totalCost
}
