export default {
  STAMP_TYPE_CHANGED : 'stamp-type-changed',
  NO_OF_SHEETS_CHANGED : 'no-of-sheets-changed',
  NO_OF_PACKS_CHANGED : 'no-of-packs-changed',
}
