import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    stampType:'',
    numberOfPacks: 0,
    numberOfSheets: 0,
    perPack: 400000,
    persheet: 150000,
    name:'',
    scn:'',
    totalCost: 0,
    pickUpAddress : '',
    pickup: 'my_branch',
   }
 })
