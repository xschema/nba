export default {
    stampType:'Personal',
    numberOfPacks: 3,
    numberOfSheets: 4,
    perPack: 400000,
    persheet: 150000,
    name:'',
    scn:'',
    totalCost: 1400000,
    pickUpAddress : '',
    pickup: 'my_branch',
    buyer: {},
    branches: [],
  }
