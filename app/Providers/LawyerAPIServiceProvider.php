<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client as Guzzle;


class LawyerAPIServiceProvider extends ServiceProvider
{

    protected $defer = true; 

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('APIClient', function ($app) {
            return new Guzzle([ 'base_uri' => 'https://www.nigerianbar.org.ng/api/api/Account/'] );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['APIClient'];
    }
}
