<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        \View::composer(['backend.ticket.index', 'backend.ticket.show'], function ($view) {
          $all_tickets = \App\Ticket::all();
          $total = count($all_tickets);
          $new = count($all_tickets->where('status','New'));
          $pending = count($all_tickets->where('status','Pending'))  + $new;
          $answered = count($all_tickets->where('status','Answered'));
          $closed = count($all_tickets->where('status','Closed'));

          return $view->with( compact('total','new','pending','answered','closed') );

        });
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
