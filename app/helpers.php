<?php
function money($value){
    return number_format($value, 2);
}

function naira($value){
    $value = money($value, 2);
    return html_entity_decode("&#8358;{$value}");
}
