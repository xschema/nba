<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public $guarded = [];

    public $timestamps = false;
}
