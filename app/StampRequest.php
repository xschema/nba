<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StampRequest extends Model
{
  use SoftDeletes;

    protected $with = ['buyer'];
    protected $guarded = [];
    protected $appends = ['details', 'buyer_name', 'branch', 'scn', 'total_cost'];
    protected $dates = ['deleted_at'];
    public function getDetailsAttribute()
    {
      $string = sprintf("%s %s, %s %s, %s",
                         $this->packs,
                         str_plural('Pack', $this->packs),
                         $this->sheets,
                         str_plural('Sheet', $this->sheets),
                         $this->type
              );
      return $string;
    }


    public function buyer()
    {
      return $this->belongsTo('App\User' , 'buyer_id');
    }


    public function getBuyerNameAttribute()
    {
      return $this->buyer->name;
    }


    public function getScnAttribute()
    {
      return $this->buyer->lawyer_account->scn;

    }


    public function getBranchAttribute()
    {
      return $this->buyer->lawyer_account->branch;
    }


    public function setPaid($transaction_id)
    {
      $this->update([
        'paid' => true,
        'paid_at' => now()->toDateTimeString(),
        'transaction_id' => $transaction_id,
        'status' => 'confirmed'
        ]);
    }


    public function getIsPaidAttribute()
    {
      return ! is_null($this->paid_at);
    }

    public function getPaymentUrlAttribute()
    {
      return url('/stamp-request-payment/'.$this->id);
    }


    public function getInvoiceUrlAttribute()
    {
      return url('/stamp-request-invoice/'.$this->id);
    }


    public function scopeUnpaid($query)
    {
      return $query->whereNull('paid_at');
    }

    public function getTotalCostAttribute()
    {
      return (int) ($this->amount + $this->charges);
    }
}
