<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $guarded = [];

    public function ticket(){ 
        return $this->belongsTo('\App\Ticket', 'ticket_id');
    }

    public static function multipleFileUpload($folder,$request, $file_name){
        $ext = $request->getClientOriginalExtension();
        $new_name = $file_name.time().'.'.$ext;

        try{
            $path = $request->storeAs($folder,$new_name,'uploads');
            return $path;
        }catch(\Exception $e){
            return false;
        }
        return false;
    }


}
