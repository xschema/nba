<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $with = ['lawyer_account'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->middle_name. ' ' . $this->last_name;
    }


    public function lawyer_account()
    {
        return $this->hasOne('App\Lawyer');
    }

    public function ticket()
    {
        return $this->hasMany('App\Ticket');
    }
    public function stampRequests()
    {
      return $this->hasMany('App\StampRequest', 'buyer_id');
    }
    public function isManager()
    {
      return $this->role === 'manager';
    }
    public function hasCart()
    {
      return $this->stampRequests()->get()->some('paid', false);
    }
}
