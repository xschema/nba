<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];
function user(){
	return $this->belongsTo('\App\User','user_id');
}

function attachment(){
	return $this->hasMany('\App\Attachment');
}

function reply(){
	return $this->hasMany('\App\Reply');
}

}

