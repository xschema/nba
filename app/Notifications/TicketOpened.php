<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Ticket;
 
class TicketOpened extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, $message_body)
    {
        $this->ticket = $ticket;
        $this->message_body = $message_body;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    
    public function toMail($notifiable)
    {
        $url = url('/manager/ticket/'.$this->ticket->ticket_number.'/'.$this->ticket->id);

        return (new MailMessage)->from($this->ticket->user->email,$this->ticket->user->name)
         ->subject($this->ticket->subject)
        ->markdown('mail.ticket.opened',['url' => $url,'message_body'=>$this->message_body]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
