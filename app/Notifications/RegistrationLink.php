<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Registration;

class RegistrationLink extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void 
     */
    public function __construct(Registration $registration, $message_body, $url, $action)
    {
        $this->registration = $registration;
        $this->message_body = $message_body;
        $this->action = $action;
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->action=='approved'){
        $url = url($this->url);

        return (new MailMessage)->from('support@nbaportal.org.ng')
         ->subject('NBA stamp Portal Registration')
        ->markdown('mail.registration.approved',['url' => $url,'message_body'=>$this->message_body]);

    }else if ($this->action =='rejected') {
        //dd('Here');
        $url = url($this->url);
        return (new MailMessage)->from('support@nbaportal.org.ng')
         ->subject('NBA stamp Portal Registration')
        ->markdown('mail.registration.rejected',['url' => $url,'name'=>$this->registration->name,'message_body'=>$this->message_body]);
    }
    else{
        
    }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
