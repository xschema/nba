<?php

namespace App\Exports;

use App\StampRequest;
use Maatwebsite\Excel\Concerns\{FromQuery, WithHeadings, WithMapping, ShouldAutoSize};

class TodayExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
  use ExportsRequests;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return StampRequest::whereDate('paid_at', today()->toDateString());
    }

  }
