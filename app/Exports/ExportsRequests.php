<?php
namespace App\Exports;

trait ExportsRequests
{
  public function headings(): array
  {
      return [
        'Branch',
        'Name',
        'SCN',
        'Practice',
        'Qty. of Packs',
        'Qty. of Sheets',
        'Expiry'
      ];
  }

  public function map ($request) : array
  {
    return [
        $request->buyer->lawyer_account->branch,
        $request->buyer->name,
        $request->buyer->lawyer_account->scn,
        $request->buyer->lawyer_account->category,
        $request->packs,
        $request->sheets,
        $request->expiry,
      ];
  }

}
