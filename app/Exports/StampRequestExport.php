<?php

namespace App\Exports;

use App\StampRequest;
use Maatwebsite\Excel\Concerns\{Exportable, FromQuery, WithHeadings, WithMapping, ShouldAutoSize};

class StampRequestExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
  use Exportable, ExportsRequests;
    /**
    * @return \Illuminate\Support\Collection
    */

  function __construct( $ids )
  {
    $this->ids = $ids;
  }

  public function query()
  {
      return StampRequest::query()->whereIn('id', $this->ids);
  }

}
