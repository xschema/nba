<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    //
    protected $guarded = [];

    public function ticket(){

    	return $this->belongsTo('\App\Ticket','ticket_id');
    }

    function user(){
		return $this->belongsTo('\App\User','user_id');
	}

	function attachment(){
	return $this->hasMany('\App\ReplyAttachment');
}

}
