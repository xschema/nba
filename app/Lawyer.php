<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lawyer extends Model
{
    //
    protected $guarded = [];
    public function user_account()
    {
      return $this->belongsTo('App\User');
    }
}
