<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Registration,Attachment,RegistrationComment,Branch};
class RegistrationController extends Controller
{
    public function index (){

        $branches = Branch::all();

        return view('auth.register', compact('branches'));
    }
    
    public function store(Request $request){
     //dd(($request->all()));
        try{
            $validate = $this->validate($request,[
                'first_name' => 'required',
                'last_name' => 'required',
                'dob' => 'required',
                'phone_number' => 'required',
                'other_number' => 'required',
                'email' => 'required|email',
                'gender' => 'required',
                'scn' => 'required',
                'year' => 'required',
                'practice_type' => 'required',
                'branch' => 'required',
                'permanent_address' => 'required',
                'office_address' => 'required',
                'card_type' => 'required',
                'card_number' => 'required',             
            ]);
        }catch (\Exception $e){
            session()->flash('alert', [
                'status' => 'danger',
                'message' => 'All Fields are required',
            ]);
            return back();
        }

        $doc_paths = [];
        if($request->hasfile('card_image'))
        {
            try{
                $i=0;
                $card_image = $request->file ("card_image");
                $doc_paths[0]= Attachment::multipleFileUpload('images/registrations',$card_image, 'reg-');

               /* foreach ($request->file ("card_image") as $card_image ) {
                    $doc_paths[$i]= Attachment::multipleFileUpload('images/registrations',$card_image, 'reg-');
                    $i++;
                }*/

            }catch(\Exception $e){
                //throw($e);
                session()->flash('alert', [
                'status' => 'danger',
                'message' => 'Error Occured, document could not be uploaded, please try again!!',
            ]);
            // return response()->json(['error'=>'Error Occured, document could not be uploaded, please try again!!']);
            return back();
            }

            if(empty($doc_paths)){
            	session()->flash('alert', [
                'status' => 'danger',
                'message' => 'Document could not be uploaded, please try again!!',
            ]);
            //return response()->json(['error'=>'Error Occured, document could not be uploaded, please try again!!']);

            return back();

            }
        }//end if request hasfile
        else{
            session()->flash('alert', [
                'status' => 'danger',
                'message' => 'Please upload the relevant document, and try again!!',
            ]);
            //return response()->json(['error'=>'Error Occured, document could not be uploaded, please try again!!']);

            return back();
        }
        
        $register = Registration::firstOrCreate([
            'salutation'=> strtoupper($request->salutation) ,
            'first_name'=>strtoupper($request->first_name) ,
            'middle_name'=>strtoupper($request->middle_name) ,
            'last_name'=>strtoupper($request->last_name) ,
            'dob'=>date_format(date_create($request->dob),'Y-m-d'),
            'phone_number'=>$request->phone_number,
            'other_number'=>$request->other_number,
            'email'=>$request->email,
            'gender'=>$request->gender,
            'scn'=>$request->scn,
            'year'=>date_format(date_create($request->year),'Y-m-d'),
            'practice_type'=>ucfirst($request->practice_type),
            'branch'=>strtoupper($request->branch),
            'permanent_address'=>ucfirst($request->permanent_address),
            'office_address'=>ucfirst($request->office_address),
            'card_type'=>$request->card_type,
            'card_number'=>$request->card_number,
            'card_image'=>$doc_paths[0],
        ]);

        if($register){
            // return response()->json(['success'=>'Reegistration successful']);
            session()->flash('alert', [
                'status' => 'success',
                'message' => 'Registration successful',
            ]);
            return back();
        }
        else{
        // return response()->json(['error'=>'Error Occured, Registration not successful, please try again!!']);
        session()->flash('alert', [
            'status' => 'danger',
            'message' => 'Error Occured, Registration not successful, please try again!!',
        ]);
        return back();
    }
    }//end store

    public function show($hash,$id){
        
        $registration = Registration::find($id);
        if(!$registration){
            abort(404);
        }

        //abort_if(count($registration)==0, 404);

        $comment = $registration->comment->where('hash',$hash)->where('active',true)->first();

        abort_if(is_null($comment), 404);
        
        //dd($comment->hash);

        //find with both record
        //dd($registration);
        $branches = Branch::all();

        return view('auth.edit-registration', compact('registration','id','comment','branches'));

    }

    public function update(Request $request, $id){
        //dd($request);
        try{
         $validate = $this->validate($request,[
                'first_name' => 'required',
                'last_name' => 'required',
                'dob' => 'required',
                'phone_number' => 'required',
                'other_number' => 'required',
                'email' => 'required|email',
                'gender' => 'required',
                'scn' => 'required',
                'year' => 'required',
                'practice_type' => 'required',
                'branch' => 'required',
                'permanent_address' => 'required',
                'office_address' => 'required',
                'card_type' => 'required',
                'card_number' => 'required',             
            ]);
        }catch (\Exception $e){
            session()->flash('alert', [
                'status' => 'danger',
                'message' => 'All Fields are required',
            ]);
            return back();
        }


        $register = Registration::where('id',$id)->update([
            'salutation'=> ucfirst($request->salutation) ,
            'first_name'=>ucfirst($request->first_name) ,
            'middle_name'=>ucfirst($request->middle_name) ,
            'last_name'=>ucfirst($request->last_name) ,
            'dob'=>date_format(date_create($request->dob),'Y-m-d'),
            'phone_number'=>$request->phone_number,
            'other_number'=>$request->other_number,
            'email'=>$request->email,
            'gender'=>$request->gender,
            'scn'=>$request->scn,
            'year'=>date_format(date_create($request->year),'Y-m-d'),
            'practice_type'=>ucfirst($request->practice_type),
            'branch'=>ucfirst($request->branch),
            'permanent_address'=>ucfirst($request->permanent_address),
            'office_address'=>ucfirst($request->office_address),
            'card_type'=>$request->card_type,
            'card_number'=>$request->card_number,
            'status'=>'Pending',
        ]);

        if($register){

            //update comment 
            $comment = RegistrationComment::where('hash',$request->hash)->update(['active'=>false]);
            // return response()->json(['success'=>'Reegistration successful']);
            session()->flash('alert', [
                'status' => 'success',
                'message' => 'Update successful',
            ]);

            return redirect()->route('registration.index');
        }

        else{
        // return response()->json(['error'=>'Error Occured, Registration not successful, please try again!!']);
        session()->flash('alert', [
            'status' => 'danger',
            'message' => 'Error Occured, Update not successful, please try again!!',
        ]);
    }
    return back();
  
        }

}
