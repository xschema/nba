<?php

namespace App\Http\Controllers;

use App\Stamp;
use Illuminate\Http\Request;

class StampController extends Controller
{
function __construct()
{
  $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function old()
    {
      $branches =  \App\NbaDb::distinct('branch')
       ->select('branch')
       ->orderBy('branch')
       ->get();
     return view('frontend.stamp.old-create', compact('branches'));
    }
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $branches = \App\NbaDb::distinct('branch')
              ->select('branch')
              ->orderBy('branch')
              ->get();
       $user = auth()->user();
       $lawyer_account = $user->lawyer_account()->first();
       $buyer = collect([$user->toArray(), $lawyer_account->toArray()])->collapse();


      return view('frontend.stamp.create', compact('buyer','branches') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stamp  $stamp
     * @return \Illuminate\Http\Response
     */
    public function show(Stamp $stamp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stamp  $stamp
     * @return \Illuminate\Http\Response
     */
    public function edit(Stamp $stamp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stamp  $stamp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stamp $stamp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stamp  $stamp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stamp $stamp)
    {
        //
    }
}
