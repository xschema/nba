<?php

namespace App\Http\Controllers;

use App\StampRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class StampRequestController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
       return view('frontend.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $branches = \App\NbaDb::distinct('branch')
              ->select('branch')
              ->orderBy('branch')
              ->get();
       $user = auth()->user();
       $lawyer_account = $user->lawyer_account()->first();
       $buyer = collect([$user->toArray(), $lawyer_account->toArray()])->collapse();


      return view('frontend.stamp.create', compact('buyer','branches') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Response $response)
    {
      try {
        $stamp_request = StampRequest::firstOrCreate([
           'buyer_id' => auth()->user()->id,
           'type' => $request->stamp_type,
           'expiry' => $request->expiry,
           'packs' => $request->packs,
           'amount' => $request->amount,
           'charges' => $request->charge,
           'sheets' => $request->sheets,
           'status' => 'unconfirmed',
           'pick_up_address' => $request->pick_up_address,
      ]);

     return response()
             ->json( [
                'created' => true,
                 'request_data' => $stamp_request,
                 'message' => 'Your request has been registered, proceed to make payments to confirm your request',
               ])
             ->setStatusCode(201);
       } catch ( \Exception $e)
       {

          return response()
             ->json( [
                'created' => false,
                 'message' => 'There was a problem storing your request',
                 'exception' => $e->getMessage(),

               ])
             ->setStatusCode(500);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StampRequest  $stampRequest
     * @return \Illuminate\Http\Response
     */
    public function show(StampRequest $stampRequest)
    {
        //
    }
    public function showPayment(StampRequest $stamp_request)
    {
      $this->authorize('view', $stamp_request);
      return view('frontend.stamp.payment', compact('stamp_request') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StampRequest  $stampRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StampRequest $stampRequest)
    {
      $stampRequest->update([
          'expiry' =>$request->expiry,
          'packs' =>$request->packs,
          'sheets' =>$request->sheets,
          'amount' =>$request->amount,
          'charges' =>$request->charges,
        ]);
        $stampRequest->save();

        return response()->json([
          'updated' => true,
        ], 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StampRequest  $stampRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(StampRequest $stamp_request)
    {
        $this->authorize('delete', $stamp_request);
        $stamp_request->delete();
        return response()->json(['deleting' => true]);

    }
    public function buyerRequests(Request $request, Response $response)
    {
      $stamp_requests = auth()->user()->stampRequests;
      return response()->json(['data' => $stamp_requests ]);
    }

    public function confirmPayment(StampRequest $stamp_request, Request $request)
    {
      $this->authorize('update', $stamp_request);
        $stamp_request->setPaid($request->transaction_id);
        return response()->json(['confirmed' => true], 201);
    }

    public function updateDeliveryStatus(Request $request)
    {
      $this->authorize('update', StampRequest::find($request->stamp_request_id) );
      $stamp_request = StampRequest::findOrFail( $request->stamp_request_id );
      $stamp_request->status = $request->status;
      $stamp_request->save();
      return response()->json(['updated' => true])->setStatusCode(201);
    }

    public function showInvoice(Request $request, StampRequest $stamp_request)
    {
      $this->authorize('view', $stamp_request);
      return view('frontend.stamp.invoice', compact('stamp_request') );
    }
}
