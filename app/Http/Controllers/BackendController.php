<?php

namespace App\Http\Controllers;
use App\Exports\StampRequestExport;
use App\{StampRequest, Ticket};
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TodayExport;

class BackendController extends Controller
{

  function __construct()
  {
    $this->middleware('auth',['support','manager']);
  }

  public function exportToday()
  {
    (StampRequest::whereDate('created_at', today()->toDateString())->where('paid', true)->get())
      ->each( function($request) {

        $request->update(['status' => 'processing']);

    });

    return Excel::download(new TodayExport, 'exports'. today()->toDateString() .'.xlsx');
  }

  public function exportRequests(Request $request)
  {
    // dd(array_values($request->requests_ids));
    $filename = 'exports/requests-' . now()->format('Y-m-d-hms') .'.xlsx';

    Excel::store(new StampRequestExport($request->requests_ids),  $filename, 'uploads');
    return response()->json([
      'url' => '/'. $filename,
       'message' => 'Succesfully Generated report, click on the download button to download the report'
     ]);
  }

  public function showPaginatedRequests()
  {
    $total_sales = StampRequest::whereNotNull('paid_at')->select('amount')->get()->sum('amount');
    return view('backend.stamp.index', compact('total_sales'));
  }

  public function AllRequests()
  {
    try{

      $data = StampRequest::whereNotNull('paid_at')->get();
      $status = 'success';

      return response()->json( compact('data', 'status') );

    } catch (\Exception $e)

    {
      return response()->json( [ 'status' => 'failed', 'debug' => $e->getMessage()] );
    }
  }

  public function index()
  {
      $tickets = Ticket::latest('created_at')->take(5)->get();
      $total_tickets = Ticket::all();
      $pending_tickets = count($total_tickets->where('status','Pending'));
      $total_tickets = count($total_tickets);
      $todays_requests = StampRequest::whereDay('paid_at', today()->format('d'))
        ->where('paid', true)
        ->get();

      $total_requests = StampRequest::select('status')->get();

      $total_requests_count = $total_requests->where('status','confirmed')->count();

      //dd($total_requests_count);

      $unprocessed_requests_count = $total_requests->where('status', 'unconfirmed')->count();
      $total_lawyers = \App\Lawyer::count();

      extract($this->getStats($todays_requests, $total_requests_count, $unprocessed_requests_count));


      $ticket_count = $total_tickets ? : 1;
      //$unprocessed_count = $unprocessed;
      $pending_ticket_percentage =  ($pending_tickets / $ticket_count) * 100;

      return view(
        'backend.index',
        compact(
          'todays_requests',
          'total_requests_count',
          'total_lawyers',
          'tickets',
          'total_tickets',
          'pending_tickets',
          'pending_ticket_percentage',
          'total_sales'
        )
      );
    }

    private function getStats($requests, $total, $unprocessed)
    {

      $stamps_count = $total ?: 1;
      $unprocessed_count = $unprocessed;
      $unprocessed_percentage =  ($unprocessed_count / $stamps_count) * 100;

      $total_sales = $this->getTotalSales($requests);
      return compact('unprocessed_percentage', 'total_sales');
    }

    private function getTotalSales( Collection $sales)
    {
      return $sales->sum('amount') / 100;
    }
}
