<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\{User,Lawyer};
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Session;
class VerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      if( auth()->user() ) return redirect('/login');
        return view('frontend.landing');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //sign up;
    public function store(Request $request)
    {
        //dd($request->category);
        //check if already exist
        $check = User::where('email',$request->email);
        //if exist
        if(!$check)
            return response()->json(["error"=>"Email already exist, please login"]);
        DB::beginTransaction();
        try{
          //use transaction db
        $scn = session('lawyer')->scn;
        $first_name =session('lawyer')->first_name;
        $middle_name =session('lawyer')->middle_name;
        $last_name =session('lawyer')->last_name;
        $email =$request->email;
        $phone_number =$request->phone_number;
        $branch =session('lawyer')->branch;
        //$year =$request->year;
        $password=$request->password;

        //dd($phone_number);

        $user = User::create([
            'email'=>$request->email,
            'role'=>$request->role,
            'password'=>bcrypt($password),
            'first_name' => $first_name,
            'middle_name' =>$middle_name,
            'last_name' =>$last_name,
            'avatar_url' =>'https://www.gravatar.com/avatar/'. md5( strtolower( $request->email ) ).'?d=mp',
        ]);

        $lawyer = Lawyer::create([
            'user_id' => $user->id,
            'scn' => $scn,
            'email'  =>$email,
            'phone_number' =>$phone_number,
            'branch' =>$branch,
            'category' =>$request->category
        ]);

        if($lawyer){
            if (Auth::attempt(['email'=>$email,'password'=>$password])){
            DB::commit();
            Session::forget('lawyer');
            return response()->json(["success"=>"Sign up successful"]);
        }
        }
    } catch(\Exception $e){
        DB::rollBack();
        dd($e);
        
         return response()->json(["error"=>"Error occured, please try again!!"]);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($scn =0)
    {
        //dd($scn);
    if($scn =='0')
        return redirect()->route('verification.index');
    try{

        //check if lawyer has registered
        $user = Lawyer::where('scn',$scn)->first();

        if($user)
            return redirect()->route('login');
        //fetch details from nbadb
        $client = new Client();

        $req = $client->request( 'GET' ,'https://www.nigerianbar.org.ng/api/api/Account/SearchUsers?str='. $scn);
        $res = json_decode($req->getBody());
        //if not found
        if(empty($res) ) {
             return redirect()->route('verification.index');
        }

        $nameParts =explode(" ",  trim(($res[0])->FullName));
        if(count($nameParts)==2)
            $nameParts= array_add($nameParts,'middle_name'," ");
        $lawyer = [];

        list($first_name, $middle_name,  $last_name) = $nameParts;
        $lawyer['scn'] = ($res[0])->SCNumber;
        $lawyer['branch'] = ($res[0])->BranchTitle;
        $lawyer['first_name'] = $first_name;
        $lawyer['middle_name'] = $middle_name;
        $lawyer['last_name'] = $last_name;
       Session(['lawyer'=>(object)$lawyer]);

       //dd(session('lawyer'));
       $lawyer = (object)$lawyer;
       return view('auth.verification', compact('lawyer'));
    }catch (\Exception $e){
        //dd($e);
         return redirect()->route('verification.index');
    }

    }
        //verify if details are correct
    /*public function verify(Request $request){

         $getBranch = trim($request->branch);
        if (Session()->has('lawyer')) {
            //$re="session found";
            session('lawyer')->branch= strtoupper($getBranch);
                return 'true';
        }
        return 'false';
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response

    public function edit($id)
    {
        //
    }
 */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response

    public function update(Request $request, $id)
    {
        //
    }
*/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response

    public function destroy($id)
    {
        //
    } */
}
