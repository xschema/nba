<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\{Lawyer,User};
use Illuminate\Support\Facades\Hash;

class LawyerProfileController extends Controller
{
	 public function __construct()
    {
        $this->middleware(['auth']);
    }
    //
    public function index(){

    	$user = Auth::user();
    	$lawyer = $user->lawyer_account;
    	$stamps  = count($user->ticket);
    	$tickets  = count($user->ticket);
    	//dd($tickets);
    	//dd($lawyer);
    	return view('frontend.profile', compact('lawyer','stamps','tickets'));
    }

    public function update(Request $request){
    	try{
            $validate = $this->validate($request,[
                'email' => 'required|email',
                'phone_number' => 'required',
            ]);

        }catch (\Exception $e){

            session()->flash('alert', [
                'status' => 'danger',
                'message' => 'All Fields are required',
            ]);

            return back();
        }
        $email = trim($request->email);

        $user = Auth::user();
        $lawyer = $user->lawyer_account;
        $lawyer = Lawyer::where('scn', $lawyer->scn)
        ->update([
        	'email' => $email,
        	'phone_number' => trim($request->phone_number),
        ]);

        $user->email = $email;
        $user->avatar_url = 'https://www.gravatar.com/avatar/'. md5( strtolower( $request->email ) ).'?d=mp';

        $user->save();

        session()->flash('alert', [
                'status' => 'success',
                'message' => 'Update successful',
            ]);
            return back();
    }

    public function changePassword(Request $request){

        try {
            $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required',
            'confirm-password' => 'required',
        ]);
        } catch (\Exception $e) {
            return response()->json(['error'=>'All fields are required!!']);
        }
        ///dd('xnnnx'); pass1234

        if($request->get('confirm-password')!=$request->get('new-password')){
            return response()->json(['error'=>'Passwords does not match!!']);
        }

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
// The passwords matches
            return response()->json(['error'=>'Your current password does not matches with the password you provided. Please try again.!!']);

        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            return response()->json(['error'=>'New Password cannot be same as your current password. Please choose a different password.!!']);
        }


//Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return response()->json(['success'=>'Password changed successfully!']);
        /*session()->flash('alert', [
                'status' => 'success',
                'message' => 'Password changed successfully!',
            ]);
            return back();*/
    }
}
