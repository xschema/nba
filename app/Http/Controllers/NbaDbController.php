<?php

namespace App\Http\Controllers;
use App\NbaDb;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class NbaDbController extends Controller
{
    public function show(Request $request){
        

        try {
            
        $client = new Client();

        $req = $client->request( 'GET' ,'https://www.nigerianbar.org.ng/api/api/Account/SearchUsers?str='. $request->scn);
        $res = json_decode($req->getBody());
        
        if(empty($res) ) {
             return response()->json(['error'=>'Data not found']);
        }
        $result = array();
        //check for two names
        $nameParts =explode(" ",  trim(($res[0])->FullName));
        if(count($nameParts)==2)
            array_add($nameParts,'middle_name'," ");

        list($first_name, $middle_name,  $last_name) = $nameParts;

        $result['first_name'] = $first_name;
        $result['middle_name'] = $middle_name;
        $result['last_name'] = $last_name;
        $result['scn'] = ($res[0])->SCNumber;

        $lawyer = array_add($result, 'success','Data found');
        return response()->json($lawyer);
        } catch (\Exception $e) {
             return response()->json(['error'=>'Please Check your Internet connection!!']);
        }
    }
}
