<?php

namespace App\Http\Controllers;
use Illuminate\Notifications\Notifiable;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\{Registration,Lawyer, User,RegistrationComment};
use App\Notifications\{RegistrationLink};
class GetRegistrationController extends Controller
{
    use Notifiable;
    function __construct()
    {
    $this->middleware('auth',['support','manager']);
    }
    //
    public function index($status ='pending'){
        $numperpage = 15;
        try {
            //for all
            if($status == 'all'){
                 $registrations = Registration::latest()->paginate($numperpage);
            }else{
                $status = ucfirst($status);
                $registrations = Registration::where('status',$status)->latest()->paginate($numperpage);
            }

        } catch (\Exception $e) {
            
            $registrations = Registration::latest()->paginate($numperpage);

        }

        $reg = Registration::all();
        $total = count($reg);
        $private = count($reg->where('status','Private'));
        $public = count($reg->where('status','Public'));
        $approved = count($reg->where('status','Approved'));
        $pending = count($reg->where('status','Pending'));
        
        return view('backend.registration.index', compact('registrations','pending','approved','public','private','total'));
    }

    public function show($id){
        $lawyer = Registration::find($id);
        
        return view('backend.registration.show', compact('lawyer'));

    }

    public function approve($id, $action ='none'){

        $registration = Registration::find($id);
        if($action=='approve'){
            DB::beginTransaction();            
            //create user
            $password = 'pass123';
            try {
                $user = User::create([
                'email'=>$registration->email,
                'role'=>'Lawyer',
                'password'=>bcrypt($password),
                'first_name' => $registration->first_name,
                'middle_name' =>$registration->middle_name,
                'last_name' =>$registration->last_name,
                'avatar_url' =>'https://www.gravatar.com/avatar/'. md5( strtolower( $registration->email ) ).'?d=mp',
            ]);

            $lawyer = Lawyer::create([
                'user_id' => $user->id,
                'scn' => $registration->scn,
                'email'  =>$registration->email,
                'phone_number' =>$registration->phone_number,
                'branch' =>$registration->branch,
                'category' =>$registration->practice_type
            ]);

             if($user && $lawyer){
                DB::commit();
                $registration->update(['status'=>'Approved']);
             }
            
            //send mail 

            try {

                $message = 'Your registration has been approved. Your Login details is as follows. password: '.$password.' username: '.$registration->email;
                //registration model,message,url,action
                $user->notify(new RegistrationLink($registration, $message,'/login','approved'));
                    
                return response()->json(['success'=>'Registration Approved']);

            } catch (\Exception $e) {
                return response()->json(['success'=>'Registration Approved, But email not sent']);    
            }
            
            //return response()->json(['success'=>'Registration Approved']);

            } catch (\Exception $e) {
                //dd($e);
                DB::rollBack();
                return response()->json(['error'=>'Error occoured, please try again!!']);
            }
            return response()->json(['error'=>'Error occoured, please try again']);
        }
        else{
            return response()->json(['error'=>'Unknown action']);
        }
        return response()->json(['error'=>'Error occoured, please try again']);
        

    }

    public function reject(Request $request){
        //dd('here');
        try {
            $registration = Registration::find($request->id);
            

            if($registration){

                //remove comment below
                $registration->update(['status'=>'Rejected']);
                
               //send mail 
            try {
                $hash= uniqid();
                if($request->edit_link=='on'){//if user should receive edit link
                    $url = 'registration/'.$hash.'/'.$request->id;
                }else{
                    $url = '/register';
                }

                $registrationComment = RegistrationComment::firstOrCreate([
                    'registration_id'=>$registration->id,
                    'staff_id'=> auth()->user()->id,
                    'comment'=>$request->comment,
                    'hash'=>$hash,
                ]);

                
                 $registration->notify(new RegistrationLink($registration,$request->comment, $url,'rejected'));

                return response()->json(['success'=>'Registration Rejected successfully']);    

            } catch (\Exception $e) {
                //dd($e);
                return response()->json(['success'=>'Registration Rejected, But email not sent']);    
            }
            
                //return response()->json(['success'=>'Registration Rejected successfully']);
            }
            else{
                return response()->json(['error'=>'Record Not found']);
            }
            
        } catch (\Exception $th) {
            //throw $th;
            return response()->json(['error'=>'Error occoured, please try again']);
        }
        
        
        return response()->json(['error'=>'Error occoured, please try again']);
        

    }
}
