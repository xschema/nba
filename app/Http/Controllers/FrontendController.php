<?php

namespace App\Http\Controllers;

use App\StampRequest;
use Illuminate\Http\{Request, Response};

class FrontendController extends Controller
{
function __construct()
{
  $this->middleware('auth');
}
    public function index( Request $request )
    {
      $stamp_requests = auth()->user()->stampRequests;
      $totalTickets = auth()->user()->ticket;
      $unpaid_requests = count($stamp_requests->where('paid', false));
      $total_stamps = count($stamp_requests);
      $total_stamps =  ($total_stamps > 0 ) ? $total_stamps : 1 ;
      $stamp_requests_count =  ( $stamp_requests->count() > 0 ) ? $stamp_requests->count() : 1 ;
      $percentage = ( $unpaid_requests  / $stamp_requests_count ) * 100 ;
      $pendingTickets = count($totalTickets->whereIn('status',['Pending','New']));
      $totalTickets = count($totalTickets);
      //dd($pendingTickets);
      return view('frontend.buyer-home', compact('stamp_requests','totalTickets','pendingTickets', 'percentage'));
    }

    public function buyerRequests(Request $request, Response $response)
    {
      $requests = auth()->user()->stampRequests;
      $cart = $requests->where('paid', false)->first();
      $stamp_requests = $requests->where('paid', true);
      return view('frontend.buyer-request', compact('stamp_requests', 'cart'));
    }

}
