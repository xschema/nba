<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\{Lawyer,User};

use DataTables;

use Illuminate\Support\Facades\DB;

class GetLawyerController extends Controller
{
    function __construct()
    {
    $this->middleware('auth',['support','manager']);
    }

    public function index(){
        $private_total = Lawyer::where('category','private')->count();
        $public_total = Lawyer::where('category','public')->count();
        $lawyer_toatal = Lawyer::count();

    	return view('backend.lawyer.index', compact('private_total','public_total','lawyer_toatal'));

    }

     /*public function getlawyerObject()
    {
        return view('datatables.eloquent.basic-object');
    }*/

    public function getlawyerDataa()
    {
        $lawyer = User::where('role','lawyer')->with('lawyer_account');

        return Datatables::of($lawyer)
        ->addColumn('action', function ($lawyer) {
                return '<a href="view-'.$lawyer->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
        ->addColumn('lawyer_account', function(User $user){
            return $user->lawyer_account->scn;
        })
        ->addColumn('fullName', function($lawyer){
            return $lawyer->name;//.' '.$lawyer->middle_name.' '.$lawyer->last_name;
        })->toJson();
        //->make(true);
    }
    public function getlawyerData(Request $request){
        $query = User::where('role','lawyer')->with('lawyer_account')->select('users.*');
        //dd($query);

            return DataTables::eloquent($query)
            ->addColumn('scn', function(User $user){
                return $user->lawyer_account->scn;//.' '.$user->middle_name.' '.$user->last_name;
            } )

            ->addColumn('phone_number', function(User $user){
                return $user->lawyer_account->phone_number;//.' '.$user->middle_name.' '.$user->last_name;
            } )

            ->addColumn('fullName', function(User $user){
                return ucwords(strtolower($user->name));//.' '.$user->middle_name.' '.$user->last_name;
            } )

            ->addColumn('action', function ($lawyer) {
                return '<a href="" class="btn btn-xs btn-primary" disabled><i class="glyphicon glyphicon-edit"></i> View</a>';
            })

            ->make(true);
    }

}
