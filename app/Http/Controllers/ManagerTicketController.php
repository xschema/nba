<?php

namespace App\Http\Controllers;
use App\{Ticket};
use Illuminate\Http\Request;

class ManagerTicketController extends Controller
{
function __construct()
{
    $this->middleware('auth',['support','manager']);
}
  public function index($query=""){

    if($query){
      $tickets = Ticket::where('status', $query)->latest()->paginate(15);
    }else{
          $tickets = Ticket::latest()->paginate(15);
    }

    //appservice provider handling tickets status stats
    return view('backend.ticket.index', compact('tickets') );
  }
    public function show($t_num, $id){
          //appservice provider handling tickets status stats
            $ticket = Ticket::where('id',$id)->where('ticket_number',$t_num)->first();
    return view('backend.ticket.show', compact('ticket'));
  }
  public function close($id){
    try {
      $ticket = Ticket::where('ticket_number', $id)->update(['status'=>'Closed']);
      return response()->json(['success'=>'Successful']);
    } catch (\Exception $e) {
      dd($e);
       return response()->json(['error'=>'Error occured']);
    }
  }
}
