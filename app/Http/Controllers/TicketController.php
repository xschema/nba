<?php

namespace App\Http\Controllers;
use Auth;
use App\{Ticket,Attachment,User};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Notifications\TicketOpened;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                //$tickets = Ticket::orderBy('created_at', 'desc')->paginate(5);
         $lawyer = Auth::user();
         //dd($lawyer);
        $tickets = Ticket::where('user_id', $lawyer->id)->orderBy('updated_at','desc')
        ->paginate(5); //
        $my_tickets = $lawyer->ticket;
        //dd($tickets);
        $total = count($my_tickets);
        $new = count($my_tickets->where('status','New'));
        $pending = count($my_tickets->where('status','Pending'))  + $new;
        $answered = count($my_tickets->where('status','Answered'));
        $closed = count($my_tickets->where('status','Closed'));
        return view('frontend.ticket.index',compact('tickets','total','pending','answered','closed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.ticket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validate = $this->validate($request,[
                'subject' => 'required',
                'message' => 'required',
            ]);
        }catch (\Exception $e){
            session()->flash('alert', [
                'status' => 'danger',
                'message' => 'All Fields are required',
            ]);

            return back();
        }

        $doc_paths=[];
        //if documents attached
        if($request->hasfile('attached'))
        {
            try{
                $i=0;
                foreach ($request->file ("attached") as $attach ) {
                    $doc_paths[$i]= Attachment::multipleFileUpload('images/attachments',$attach, 'ticket');
                    $i++;
                }
            }catch(\Exception $e){
                throw($e);
            }
            if(count($doc_paths)<1){

                session()->flash('alert', [
                'status' => 'danger',
                'message' => 'Document could not be uploaded, please try again!!',
            ]);
            return back();
        }
        }//end if request hasfile

        $unique_code = Hash::make(str_random(10));
        $unique_code = str_replace(array('.','$','/'), '',$unique_code);
        $ticket_key =substr($unique_code,-10,10);
        $request = array_add($request,'user_id', Auth::user()->id);
        $request = array_add($request,'ticket_key', $ticket_key);

        $ticket = Ticket::firstOrCreate([
            'user_id'=>$request->user_id,
            'ticket_number'=>$request->ticket_key,
            'subject'=>$request->subject,
            'message'=>$request->message,
            'status'=> 'Pending'
        ]);

        //if document exits
        if($doc_paths){
            foreach ($doc_paths as $doc_path){
                //dd($ticket->id);
                $upload = Attachment::create([
                    'ticket_id' =>$ticket->id,
                    'path' => $doc_path
                ]);
            }
        }

        $user = User::find(1);
        //dd($ticket);
        //$ticket = add($ticket);
        $message_body = "New ticket has been opend, please click the button below to respond to it.";
        $user->notify(new TicketOpened ($ticket,$message_body));

        session()->flash('alert', [
                'status' => 'success',
                'message' => 'Ticket Created Successfully',
            ]);
        return back();
        //return response()->json(['Success'=>'Ticket Created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($t_num, $id)
    {
         $lawyer = Auth::user();
        $my_tickets = $lawyer->ticket; //
         $total = count($my_tickets);
        $new = count($my_tickets->where('status','New'));
        $pending = count($my_tickets->where('status','Pending')) + $new;
        $answered = count($my_tickets->where('status','Answered'));
        $closed = count($my_tickets->where('status','Closed'));
        $ticket = Ticket::where('id',$id)->where('ticket_number',$t_num)->first();

         return view('frontend.ticket.show',compact('ticket','total','pending','answered','closed'));
    }


}
