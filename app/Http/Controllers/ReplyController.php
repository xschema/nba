<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\{Attachment,Ticket,Reply, ReplyAttachment, User};
use App\Notifications\{TicketOpened,TicketReplied};

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function store(Request $request)
    {
        try{
            $validate = $this->validate($request,[
                'message' => 'required',
            ]);
        }catch (\Exception $e){
        	session()->flash('alert', [
                'status' => 'danger',
                'message' => 'All Fields are required',
            ]);

            return back();

        }

        $doc_paths=[];
        //if documents attached
        if($request->hasfile('attached'))
        {
            try{
                $i=0;
                foreach ($request->file ("attached") as $attach ) {
                    $doc_paths[$i]= Attachment::multipleFileUpload('images/attachments',$attach, 'reply');
                    $i++;
                }

            }catch(\Exception $e){
                //throw($e);
                session()->flash('alert', [
                'status' => 'danger',
                'message' => 'Document could not be uploaded, please try again!!',
            ]);

            return back();
            }

            if(count($doc_paths)<1){
            	session()->flash('alert', [
                'status' => 'danger',
                'message' => 'Document could not be uploaded, please try again!!',
            ]);

            return back();

            }
        }//end if request hasfile
        //get ticket id

        $ticket = Ticket::where('ticket_number', $request->ticket_number)->first();
		//dd($ticket->id);
        $reply = Reply::firstOrCreate([
            'user_id'=>Auth::user()->id,
            'ticket_id'=>$ticket->id,
            'message'=>$request->message
        ]);
        //if not a lawyer update ticket as answered
        if(strtolower(Auth::user()->role) !='lawyer'){
          $ticket->update(['status'=>'Answered']);
          //send mail to customer
          $user = $ticket->user;

            $user->notify(new TicketReplied ($ticket));
    
        }else{
            $user = $ticket->user;
            $message_body = 'Ticket ID '.$ticket->ticket_number.' was replied to. Please click the link below to view';
            $user->notify(new TicketOpened ($ticket,$message_body));
        }

        //if document exits
        if($doc_paths){
            foreach ($doc_paths as $doc_path){
                //dd($ticket->id);
                $upload = ReplyAttachment::create([
                    'reply_id' =>$reply->id,
                    'path' => $doc_path
                ]);
            }
        }
        session()->flash('alert', [
                'status' => 'success',
                'message' => 'Reply Sent Successfully',
            ]);
        return back();

    }
}
