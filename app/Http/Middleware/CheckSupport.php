<?php

namespace App\Http\Middleware;

use Closure;

class CheckSupport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth()->user()->role != 'support') {
            return redirect('/logout');
        }

        return $next($request);
    }
}
