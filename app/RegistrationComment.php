<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationComment extends Model
{
    //
        protected $guarded =[];

    public function registration(){

    	return $this->belongsTo('\App\Registration','registration_id');
    } 
}
