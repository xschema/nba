<?php

namespace App\Policies;

use App\User;
use App\StampRequest;
use Illuminate\Auth\Access\HandlesAuthorization;

class StampRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the stamp request.
     *
     * @param  \App\User  $user
     * @param  \App\StampRequest  $stampRequest
     * @return mixed
     */
    public function view(User $user, StampRequest $stampRequest)
    {
        return ($user == $stampRequest->buyer) || $user->isManager() ;
    }

    /**
     * Determine whether the user can create stamp requests.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the stamp request.
     *
     * @param  \App\User  $user
     * @param  \App\StampRequest  $stampRequest
     * @return mixed
     */
    public function update(User $user, StampRequest $stampRequest)
    {
        return $user->can('view', $stampRequest);
    }

    /**
     * Determine whether the user can delete the stamp request.
     *
     * @param  \App\User  $user
     * @param  \App\StampRequest  $stampRequest
     * @return mixed
     */
    public function delete(User $user, StampRequest $stampRequest)
    {
        return  $user->isManager();
    }

    /**
     * Determine whether the user can restore the stamp request.
     *
     * @param  \App\User  $user
     * @param  \App\StampRequest  $stampRequest
     * @return mixed
     */
    public function restore(User $user, StampRequest $stampRequest)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the stamp request.
     *
     * @param  \App\User  $user
     * @param  \App\StampRequest  $stampRequest
     * @return mixed
     */
    public function forceDelete(User $user, StampRequest $stampRequest)
    {
        //
    }
}
