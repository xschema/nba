<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplyAttachment extends Model
{
    //
    protected $guarded = [];

    public function reply(){
        return $this->belongsTo('\App\Reply', 'reply_id');
    }
}
