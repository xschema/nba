<?php

namespace App;

use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
	use Notifiable;
    //
    protected $guarded =[];

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->middle_name. ' ' . $this->last_name;
    }
    function comment(){
    return $this->hasMany('\App\RegistrationComment');
}

}
