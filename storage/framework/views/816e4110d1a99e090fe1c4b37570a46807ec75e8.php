<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('assets/images/favicon.png')); ?>">
    <title>NStamp| <?php echo $__env->yieldContent('title'); ?></title>
    
<!-- Dashboard 1 Page CSS 
 <link href="<?php echo e(asset('assets/node_modules/register-steps/steps.css')); ?>" rel="stylesheet">-->

<link href="<?php echo e(asset('dist/css/pages/login-register-lock.css')); ?>" rel=" stylesheet">
    
<link href="<?php echo e(asset('dist/css/style.min.css')); ?>" rel=" stylesheet">
   
    
    <?php echo $__env->yieldPushContent('styles'); ?>

</head> 
<body class="horizontal-nav skin-green  fixed-layout">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">NBA Stamp Purchase Website</p>
        </div>
    </div>

    <!-- Main wrapper - style you can find in pages.scss -->     

       
    <section id="wrapper">
        <?php echo $__env->yieldContent('content'); ?>
 
    </section> <!-- End Page wrapper  -->

 <!-- <footer class="footer">
            © 2018 stamp.nbanigeria.org.ng <span class="pull-right">Design and Powered by <a href="">A4mation</a></span>
        </footer>

js goes here-->

<script src="<?php echo e(asset('assets/node_modules/jquery/jquery-3.2.1.min.js')); ?>"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo e(asset('assets/node_modules/popper/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>        

<?php echo $__env->yieldPushContent('scripts'); ?>

<script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    
    /*$(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });*/
    
    </script>
</body>

</html>
