<!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                        aria-expanded="false"><img src="<?php echo e(auth()->user()->avatar_url); ?>" alt="user-img" class="img-circle"><span
                            class="hide-menu"><?php echo e(auth()->user()->name); ?></span></a>
                    <ul aria-expanded="false" class="collapse">
                       
                        <li>
                          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                            <?php echo csrf_field(); ?>
                          </form>
                          <a href="<?php echo e(route('logout')); ?>"
                          onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();" >
                            <i class="fa fa-power-off"></i>
                             Logout
                          </a>
                        </li>
                    </ul>
                </li>
                <li> <a class="waves-effect waves-dark" href="<?php echo e(route('backend.index')); ?>"><i class="icon-speedometer"></i>
                        <sspan class="hide-menu">Dashboard</span>
                    </a>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="ti-palette"></i><span class="hide-menu">Stamp Requests</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo e(route('all-requests')); ?>">All Requests</a></li>
<!--                         <li><a href="">Export Pending</a></li>
                     -->
                   </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="ti-email"></i><span class="hide-menu">Messages</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo e(route('backend.ticket.index')); ?>">Complaint</a></li>
                    </ul>
                </li>

                <li> <a class="waves-effect waves-dark" href="<?php echo e(route('backend-registration')); ?>">
                    <i class="fa fa-users"></i>
                        <span class="hide-menu">Registration</span>
                    </a>
                </li>
               
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
