<div class="alert alert-<?php echo e($flash['status']); ?> mt-2 mx-auto" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="la la-close"></span>
    </button>
    <div class="ks-inline-image-block">
        <?php echo e($flash['message']); ?> 
    </div>
</div>     