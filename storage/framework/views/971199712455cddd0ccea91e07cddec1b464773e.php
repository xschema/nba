<?php $__env->startSection('title','Profile'); ?>
<?php $__env->startPrepend('styles'); ?>
<!-- Custom CSS  -->
    <link href="<?php echo e(asset('assets/node_modules/sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css">

<?php $__env->stopPrepend(); ?>

<?php $__env->startSection('content'); ?>

<div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Profile</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo e(route('frontend.index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active">Profile</li>
                            </ol>

                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-12">
                        <div class="row mb-4 pt-2 pl-2">
                            <div class="col-sm-8 ">
                                <?php if($flash = session('alert')): ?>
                                    <?php echo $__env->make('partials.alert', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    $name = Auth::user()->name;
                    $avatar_url = Auth::user()->avatar_url;
                    $email = Auth::user()->email;
                    $phone_number = $lawyer->phone_number;
                    $branch = $lawyer->branch;
                    $year = $lawyer->year;
                    ?>


                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="<?php echo e($avatar_url); ?>" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10"><?php echo e($name); ?></h4>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4">
                                        	<a href="javascript:void(0)" class="link"> Total Tickets
                                        		<i class="mdi mdi-comment-text-outline"></i>
                                        		<font class="font-medium"><?php echo e($tickets); ?></font>
                                        	</a>
                                        </div>
                                        <div class="col-4">
                                        	<a href="javascript:void(0)" class="link"> Total Purchase
                                                <i class="mdi mdi-chart-bar"></i> <font class="font-medium"><?php echo e($stamps); ?></font>
                                        	</a>
                                        </div>
                                    </div>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body">
                            	<small class="text-muted">Email address </small>
                                <h6><?php echo e($email); ?></h6> <small class="text-muted p-t-30 db">Phone</small>
                                <h6><?php echo e($phone_number); ?></h6> <small class="text-muted p-t-30 db">Branch</small>
                                <h6><?php echo e($branch); ?></h6>
                                <br/>

                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">

                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#edit-profile" role="tab">Profile</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#change-password" role="tab">Change Password</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">

                                <!--second tab-->

                                <div class="tab-pane active" id="edit-profile" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" method="post" action="<?php echo e(route('profile.update')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <div class="form-group">
                                                <label class="col-md-12">Full Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="<?php echo e($name); ?>" class="form-control form-control-line" disabled="disabled">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" class="form-control form-control-line" name="email" id="email" value="<?php echo e($email); ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-12">Phone No</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="phone_number" id="phone_number" class="form-control form-control-line" value="<?php echo e($phone_number); ?>">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update Profile</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>

                                <div class="tab-pane" id="change-password" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material ajaxForm"  method="post" action="<?php echo e(route('profile.changePassword')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <div class="form-group">
                                                <label class="col-md-12">Current Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="current-password" id="current-password" class="form-control form-control-line" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">New Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="new-password" id="new-password" class="form-control form-control-line" required="required">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-12">Confirm Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="confirm-password" id="confirm-password" class="form-control form-control-line" required="required">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-warning">Change Password </button>
                                                </div>
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->


<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<!--morris JavaScript -->
    <script src="<?php echo e(asset('assets/node_modules/raphael/raphael-min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/sparkline/jquery.sparkline.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/sweetalert/sweetalert.min.js')); ?>"></script>

    <script src="<?php echo e(asset('js/script.js')); ?>"></script>

    <!--Custom JavaScript -->
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>