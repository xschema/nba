    
<?php $__env->startSection('title','Open Ticket'); ?>
<?php $__env->startPush('styles'); ?>

    <link href="<?php echo e(asset('assets/node_modules/sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo e(asset('assets/node_modules/dropzone-master/dist/dropzone.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('dist/css/pages/inbox.css')); ?>" rel="stylesheet">

<style type="text/css">
    .list-group .badge{
        float:right;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

 <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Complanit</h4>
    </div>

    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo e(route('frontend.index')); ?>">Home</a></li>
                <li class="breadcrumb-item "><a href="<?php echo e(route('ticket.index')); ?>">My Complanit</a></li>
            </ol>
            <a href="<?php echo e(route('ticket.create')); ?>" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Open Complanit</a>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card col-lg-9" style="margin:auto; top: 20px">
            <div class="row">
                <?php if($flash = session('alert')): ?>
                <div class="col-md-12" id="flash-message">
                    <div class="row mb-4 pt-2 pl-2">
                            
                                <?php echo $__env->make('partials.alert', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            
                    </div>
                </div> 
                <?php endif; ?>
                <div class="col-lg-9 col-md-8">
                   <div class="card-body">
				        <h4 class="card-title">Compose New Message</h4>
				        <form action="<?php echo e(route('ticket.store')); ?>" method="post" enctype="multipart/form-data">
				        	<?php echo csrf_field(); ?>
					        <div class="form-group">
					            <input class="form-control" name="subject" placeholder="Subject:" required="required">
					        </div>
					        <div class="form-group">
					            <textarea class="textarea_editor form-control" name="message" rows="5" placeholder="Enter text ..." required="required"></textarea>
					        </div>

					        <h4><i class="ti-link"></i> Attachment</h4>

					        <!-- <form action="#" class="dropzone"> -->
					            <div class="fallback">
					                <input name="attached[]" type="file" multiple />
					            </div>
					        <!-- </form> -->

                            <div class="form-group m-t-20">
					           <button type="submit" class="btn btn-success"> Send</button>
					       </div>
					    </form>
				    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<!--morris JavaScript -->
    <script src="<?php echo e(asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/sparkline/jquery.sparkline.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/sweetalert/sweetalert.min.js')); ?>"></script>
     <script src="<?php echo e(asset('assets/node_modules/dropzone-master/dist/dropzone.js')); ?>"></script>

     <script type="text/javascript">
         setTimeout(function(){
          $('#flash-message').hide('slow')
        }, 3000)
     </script>
<?php $__env->stopPush(); ?>



<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>