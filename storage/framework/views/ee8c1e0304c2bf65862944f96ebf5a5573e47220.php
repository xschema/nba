<?php $__env->startSection('title','Dashboard'); ?>
<?php $__env->startPrepend('styles'); ?>
<!-- Custom CSS  -->
    <link href="assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
<?php $__env->stopPrepend(); ?>

<?php $__env->startSection('content'); ?>
<!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">My Dashboard</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->



                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3>
                                            <p class="text-muted">My Stamp Requests</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary"><?php echo e($stamp_requests->count()); ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3>
                                            <p class="text-muted">Unpaid Requests</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-danger"><?php echo e($stamp_requests->where('paid', false)->count()); ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo e($percentage); ?>%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text-outline"></i></h3>
                                            <p class="text-muted"> My Tickets </p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-info"> <?php echo e($totalTickets); ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text"></i></h3>
                                            <p class="text-mute">Pending Tickets</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-warning"><?php echo e($pendingTickets); ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 50%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- Comment - table -->
                <!-- ============================================================== -->
                <div class="row">

                    <!-- ============================================================== -->
                    <!-- Table -->
                    <!-- ============================================================== -->
                  <div class="col-sm-8">
                   <div class="card border-success">
                            <div class="card-header bg-success">
                                <h4 class="m-b-0 text-white">Quick Actions</h4></div>
                            <div class="card-body">
                                <h3 class="card-title">Welcome to NBA Stamps Purchase Platform</h3>
                                <p class="card-text mb-5">From here you can quicky navigate through the system.</p>
                                <p class="card-text text-center mt-3 text-uppercase">What would you like to do now ?</p>
                                <div class="row button-group mx-auto mb-5 pt-5">
                                    <div class="col-sm-6 col-lg-3">
                                      <a href="<?php echo e(route('stamp-requests.create')); ?>">
                                         <button type="button" class="btn btn-block btn-outline-success">Buy Stamps</button>
                                      </a>

                                    </div>
                                    <div class="col-sm-6 col-lg-3">
                                        <a href="<?php echo e(route('buyer-requests')); ?>">
                                          <button type="button" class="btn btn-block btn-outline-success">Make Payment</button>
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-lg-3">
                                         <a href="<?php echo e(route('buyer-requests')); ?>">
                                          <button type="button" class="btn btn-block btn-outline-success">See my Requests</button>
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-lg-3">
                                         <a href="<?php echo e(route('ticket.create')); ?>">
                                          <button type="button" class="btn btn-block btn-outline-success">Open Ticket</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="<?php echo e(auth()->user()->avatar_url); ?>" class="img-circle" width="150">
                                    <h4 class="card-title m-t-10"><?php echo e(auth()->user()->name); ?></h4>
                                    <h6 class="card-subtitle"><?php echo e(auth()->user()->lawyer_account->branch); ?></h6>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body mx-auto">
                              <small class="text-muted text-center">Email address </small>
                                <h6><?php echo e(auth()->user()->email); ?></h6> <small class="text-muted text-center p-t-30 db">Phone</small>
                                <h6><?php echo e(auth()->user()->lawyer_account->phone_number); ?></h6>
                            </div>
                        </div>
                    </div>
                  
                    
                </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<!--morris JavaScript -->
    <script src="<?php echo e(asset('assets/node_modules/raphael/raphael-min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/morrisjs/morris.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')); ?>"></script>
    <!-- Popup message jquery -->
    <script src="<?php echo e(asset('assets/node_modules/toast-master/js/jquery.toast.js')); ?>"></script>
    <!-- Chart JS -->
    <script src="<?php echo e(asset('dist/js/dashboard1.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/toast-master/js/jquery.toast.js')); ?>"></script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.frontend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>