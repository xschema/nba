<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('assets/images/favicon.png')); ?>">
    <title>NBA Stamp| <?php echo $__env->yieldContent('title'); ?></title>

<!-- Dashboard 1 Page CSS -->
 <link href="<?php echo e(asset('dist/css/style.min.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('dist/css/pages/dashboard1.css')); ?>" rel="stylesheet">


    <?php echo $__env->yieldPushContent('styles'); ?>

</head>
<body class="horizontal-nav skin-green-dark  fixed-layout">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">NBA Stamp Purchase Website</p>
        </div>
    </div>

    <!-- Main wrapper - style you can find in pages.scss -->

    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo e(route('frontend.index')); ?>">
                        <!-- Logo icon --><b>
                        <img src="<?php echo e(asset('img/logo.png')); ?>" alt="homepage" width="60" class="light-logo" />
                        </b>

                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark"
                                href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                        <!-- ============================================================== -->

                    </ul>

                </div>
            </nav>
        </header>

         <aside class="left-sidebar">
            <?php echo $__env->make('partials.frontend_navbar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
        </aside>


    <div class="page-wrapper">

        <div class="container-fluid">

            <?php echo $__env->yieldContent('content'); ?>

            <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">

                        <div class="rpanel-title"> Customize theme <span><i class="ti-close right-side-toggle"></i></span>
                        </div>

                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-skin="skin-default" class="default-theme working">1</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-skin="skin-default-dark" class="default-dark-theme ">7</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-skin="skin-megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <!-- End Right sidebar -->

        </div><!-- End Container fluid  -->
    </div> <!-- End Page wrapper  -->

  <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                        <a href="<?php echo e(route('terms-conditions')); ?>" target="_blank">Terms and Conditions</a>
                </div>
                <div class="col-md-4 col-lg-4"> © 2018 nbaportal.org.ng</div>
                <div class="col-md-4 col-lg-4"> Design and Powered by <a href="#">Strataflex</a></div>
      
            </div>
        </div>
    
    </footer>
</div><!--end main wrapper-->

<!--js goes here-->

<script src="<?php echo e(asset('assets/node_modules/jquery/jquery-3.2.1.min.js')); ?>"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo e(asset('assets/node_modules/popper/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo e(asset('dist/js/perfect-scrollbar.jquery.min.js')); ?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo e(asset('dist/js/waves.js')); ?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo e(asset('dist/js/sidebarmenu.js')); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo e(asset('dist/js/custom.min.js')); ?>"></script>
<?php echo $__env->yieldPushContent('scripts'); ?>
</body>

</html>
