<?php $__env->startSection('title','View Registration'); ?>
<?php $__env->startPrepend('styles'); ?>
<link href="<?php echo e(asset('assets/node_modules/footable/css/footable.core.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/node_modules/bootstrap-select/bootstrap-select.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/node_modules/sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css">
    <!-- Page CSS -->
    <link href="<?php echo e(asset('dist/css/pages/contact-app-page.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('dist/css/pages/footable-page.css')); ?>" rel="stylesheet">
    <!-- Custom CSS -->
<?php $__env->stopPrepend(); ?>

<?php $__env->startSection('content'); ?>         

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Admin Dashboard</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo e(route('backend.index')); ?>">Dashboard</a></li>
                <li class="breadcrumb-item active">Registration</li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- .left-right-aside-column-->
            <div class="contact-page-aside">
                <!-- .left-aside-column-->
                <div class="left-aside bg-light-part">
                    <ul class="list-style-none">
                        <li class="box-label"><a class="ajaxLink" href="<?php echo e(route('backend-registration.status','all')); ?>">All Registration <span><?php echo e($total); ?></span></a></li>
                        <li class="divider"></li>
                         <li><a class="ajaxLink" href="<?php echo e(route('backend-registration.status','pending')); ?>">Pending <span><?php echo e($pending); ?></span></a></li>
                                                 
                         <li><a class="ajaxLink" href="<?php echo e(route('backend-registration.status','public')); ?>">Public <span><?php echo e($public); ?></span></a></li>
                        <li><a class="ajaxLink" href="<?php echo e(route('backend-registration.status','private')); ?>">Private <span><?php echo e($private); ?></span></a></li>
                        <li><a class="ajaxLink" href="<?php echo e(route('backend-registration.status','approved')); ?>" style="color:green;">Approved <span><?php echo e($approved); ?></span></a></li>
                        
                    </ul>
                </div>
                <!-- /.left-aside-column-->
                <div class="right-aside " style="margin-bottom:30px;">
                    <div class="right-page-header">
                        <div class="d-flex">
                            <div class="align-self-center">
                                <h4 class="card-title m-t-10">Lawyers Registration List </h4></div>
                            <div class="ml-auto">
                                <input type="text" id="demo-input-search2" placeholder="search contacts" class="form-control"> </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        
                        <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                            <thead>
                                <tr>
                                    <th>SCN</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Practice Type</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($registrations)): ?>
                                    <?php $__currentLoopData = $registrations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="ajaxLink" href="<?php echo e(route('backend-registration.show',$list->id)); ?>" style="cursor: pointer;">
                                        <td><?php echo e($list->scn); ?></td>
                                        <td>
                                            <?php echo e($list->salutation.' '.$list->name); ?>

                                        </td>
                                        <td><?php echo e($list->email); ?></td>
                                        <td><?php echo e($list->phone_number); ?></td>
                                        <td><span class="label <?php echo e($list->practice_type); ?>"><?php echo e($list->practice_type); ?></span> </td>
                                        
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>                                                       
                                
                            </tbody>
                                                        
                        </table>
                        <?php echo e($registrations->links()); ?>

                    </div>
                    <!-- .left-aside-column-->
                </div>
                <!-- /.left-right-aside-column-->
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('assets/node_modules/sweetalert/sweetalert.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/script.js')); ?>"></script>

    <script>
     $('.Public').addClass('label-danger') ; 
     $('.Private').addClass('label-success');

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>