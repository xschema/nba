<?php $__env->startSection('title','Dashboard'); ?>
<?php $__env->startPrepend('styles'); ?>
<!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Dashboard 1 Page CSS -->
<link href="dist/css/pages/dashboard1.css" rel="stylesheet">
<style type="text/css">
  .badge-default {
    background-color: #737980;
    color: white;
}
</style>
<?php $__env->stopPrepend(); ?>

<?php $__env->startSection('content'); ?>
<!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Admin Dashboard</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->



                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3> 
                                            <p class="text-muted">Total Lawyer</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary"><?php echo e($total_lawyers); ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3>
                                            <p class="text-muted">Stamp Request</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary"><?php echo e($total_requests_count); ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-cyan" role="progressbar" style="width:%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text-outline"></i></h3>
                                            <p class="text-muted"> New Tickets </p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-danger"><?php echo e($pending_tickets); ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo e($pending_ticket_percentage); ?>%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="mdi mdi-comment-text"></i></h3>
                                            <p class="text-mute">Total Tickets</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-success"><?php echo e($total_tickets); ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- Comment - table -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- Comment widgets -->
                    <!-- ============================================================== -->
                    <?php if(auth()->user()->role == 'manager'): ?>
                      <div class="col-lg-5">
                    <?php else: ?>
                      <div class="col-lg-12">
                    <?php endif; ?>

                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Recent Tickets</h5>
                            </div>
                            <!-- ============================================================== -->
                            <!-- Comment widgets -->
                            <!-- ============================================================== -->
                            <div class="comment-widgets">
                               <!-- Comment Row -->
                               <?php if(count($tickets)>0): ?>
                                <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <a href="<?php echo e(route('backend.ticket.show',[ "t_num" => $ticket->ticket_number, 'id' =>$ticket->id])); ?>">

                                <div class="d-flex no-block comment-row">
                                    <div class="p-2"><span class="round"><img src="<?php echo e($ticket->user->avatar_url); ?>" alt="<?php echo e($ticket->user->name); ?>"                     width="50"></span></div>
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><?php echo e($ticket->user->name); ?></h5>
                                        <p class="m-b-10 text-muted"><?php echo e(str_limit($ticket->message, 80,'...')); ?></p>
                                        <div class="comment-footer">
                                            <span class="text-muted pull-right"><?php echo e($ticket->updated_at->diffForHumans()); ?></span> <span class="badge badge-pill badge-<?php echo e($ticket->status); ?>"><?php echo e($ticket->status); ?></span>
                                        </div>
                                    </div>
                                </div>
                                  </a>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                <p class="text-center">No Ticket found</p>
                                 <?php endif; ?>



                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Table -->
                    <!-- ============================================================== -->
                    <div class="col-lg-7">
                        <div class="card">
                            <?php if(Auth()->user()->role =='manager'): ?>
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <h5 class="card-title">Stamp Request Overview</h5>
                                        <h6 class="card-subtitle">Recent Purchases</h6>
                                    </div>
                                    <?php if(count($todays_requests)): ?>
                                      <div class="ml-auto">
                                        <a id="export-today-button" class="btn btn-success">Export</a>
                                      </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="card-body bg-light">
                                <div class="row">
                                    <div class="col-6">
                                        <h3>Today <?php echo e(date('d,M, Y')); ?> </h3>
                                        <h5 class="font-light m-t-0">Report for today</h5>
                                    </div>
                                    <div class="col-6 align-self-center display-6 text-right">
                                        <h2 class="text-success"><?php echo e(naira($total_sales)); ?></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S/N</th>
                                            <th>NAME</th>
                                            <th>QUANTITIES</th>
                                            <th>DATE</th>
                                            <th>AMOUNT</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $__currentLoopData = $todays_requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $request): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td class="text-center"><?php echo e($loop->iteration); ?></td>
                                            <td class="txt-oflo"><?php echo e($request->buyer->name); ?></td>
                                            <td>
                                              <span >
                                                <?php echo e($request->details); ?>

                                              </span>
                                            </td>
                                            <td > <?php echo e($request->created_at->diffForHumans()); ?></td>
                                            <td><span class="text-success"> <?php echo e(naira($request->amount/100)); ?></span></td>
                                            
                                        </tr>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php if (! ( count($todays_requests) )): ?>
                                  <h3 class="text-center  my-5">There are no paid requests today</h3>
                                <?php endif; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
      $('.badge-New').addClass('badge-success');
      $('.badge-Pending').addClass('badge-danger');
      $('.badge-Answered').addClass('badge-info');
      $('.badge-Closed').addClass('badge-default');

      $('#export-today-button').on('click', function(){

        $el =  $(this)

        $el.text('Processing...')

        $.get('exports/today')
          .then(data=>{
            $el.text('Export')
          })
          .catch(err => {
            console.log(err)
          })
      })

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>