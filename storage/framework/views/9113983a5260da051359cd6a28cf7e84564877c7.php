<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Nigeria Bar Association">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('assets/images/favicon.png')); ?>">
    <title>Admin| <?php echo $__env->yieldContent('title'); ?></title>

    <!-- Custom CSS  -->
    <link href="<?php echo e(asset("dist/css/style.min.css")); ?>" rel="stylesheet">

    <?php echo $__env->yieldPushContent('styles'); ?>

</head>

<body class="skin-green-dark fixed-layout">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">NBA Stamp Purchase Portal</p>
        </div>
    </div>

    <!-- Main wrapper - style you can find in pages.scss -->

    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo e(route('backend.index')); ?>">
                        <b>
                        <img src="<?php echo e(asset('img/logo.png')); ?>" alt="homepage" width="60" class="light-logo" />
                        </b>

                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark"
                                href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                        <!-- ============================================================== -->

                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item right-side-toggle"> <a class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i
                                    class="ti-settings"></i></a></li>
                    </ul>
                </div>
            </nav>
        </header>

         <aside class="left-sidebar">
            <?php echo $__env->make('partials.backend_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
        </aside>


    <div class="page-wrapper">

        <div class="container-fluid">

            <?php echo $__env->yieldContent('content'); ?>

            <!-- .right-sidebar -->


                <!-- End Right sidebar -->


        </div><!-- End Container fluid  -->
    </div> <!-- End Page wrapper  -->

  <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                        <a href="<?php echo e(route('terms-conditions')); ?>" target="_blank">Terms and Conditions</a>
                </div>
                <div class="col-md-4 col-lg-4"> © 2018 nbaportal.org.ng</div>
                <div class="col-md-4 col-lg-4"> Design and Powered by <a href="#">Strataflex</a></div>
      
            </div>
        </div>
    
    </footer>
</div><!--end main wrapper-->

<!--js goes here-->

<script src="<?php echo e(asset("assets/node_modules/jquery/jquery-3.2.1.min.js")); ?>"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo e(asset("assets/node_modules/popper/popper.min.js")); ?>"></script>
    <script src="<?php echo e(asset("assets/node_modules/bootstrap/dist/js/bootstrap.min.js")); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo e(asset("dist/js/perfect-scrollbar.jquery.min.js")); ?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo e(asset("dist/js/waves.js")); ?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo e(asset("dist/js/sidebarmenu.js")); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo e(asset("dist/js/custom.min.js")); ?>"></script>
<?php echo $__env->yieldPushContent('scripts'); ?>
</body>

</html>
