<?php $__env->startSection('title','Registration'); ?>
<?php $__env->startPush('styles'); ?>
<link href="<?php echo e(asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo e(asset('assets/node_modules/dropify/dist/css/dropify.min.css')); ?>">

<style>
label{
    float: left;
}
#msform input{
    font-size: 15px;
}

#msform input{
    padding: 10px;
}
#eliteregister li{
    width: 24.33%;
}
#wrapper {
    /* width: 100%; */
    overflow-x: scroll;
    }
    .result{
        font-weight: bold;
        text-transform: uppercase;
        padding-left: 5px;

    }
    hr{
        margin-top: 0rem;
    }
    .prev-row{
        width: 100%;
        max-height: 100px;
        display: block;
        background-color: #d2c8c833;
        padding-left: 5px;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
 
<div class="register-box">
   

    <div class="">
        <a href="<?php echo e(route('register')); ?>" class="text-center"><img src="<?php echo e(asset('img/logo.png')); ?>" alt="NBA" width="80" /></a>
        <a href="<?php echo e(route('verification.index')); ?>" style="color: #326936"> <i class="fa fa-long-arrow-left"></i> Return Back </a>
        <h3 class="text-center" style="">Lawyer's Information Confirmation</h3>
        <!-- multistep form -->
    <form id="msform" method="POST" class="" action="<?php echo e(route('registration.store')); ?>" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <!-- progressbar -->
            <ul id="eliteregister">
                <li class="active">Step 1</li>
                <li>Step 2 </li>
                <li>Step 3</li>
                <li>Step 4</li>
            </ul>

            <div class="row pt-2 pl-2">
                <div class="col-sm-12" id="flash-message">
                    <?php if($flash = session('alert')): ?>
                        <?php echo $__env->make('partials.alert', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Step 1</h2>

                <div class="row">
                    <div class="col-md-6">
                        <label for="salutation">Salutation</label>

                    <select class="custom-select form-control" id="salutation" name="salutation" style="margin-bottom: 18px" required>

                        <option value="Barr" selected>Barr</option>
                        <option value="SAN">SAN</option>
                    </select>

                    </div>
                    <div class="col-md-6">
                        <label>First Name</label>
                        <input type="text" name="first_name" placeholder="First Name" required />
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <label for="middle Name">Middle Name</label>
                        <input type="text" name="middle_name" placeholder="Middle Name" />
                    </div>
                    <div class="col-md-6">
                        <label for="Surname Name">Last Name</label>
                <input type="text" name="last_name" placeholder="Surname" required />
                    </div>
                </div>          

                <div class="row">
                    <div class="col-md-6">
                        <label for="gender">Gender</label>

                        <select class="custom-select form-control" id="gender" name="gender" style="margin-bottom: 18px" required>
                        <option value="">Choose Category</option>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>
                    </select>
                    </div>

                    <div class="col-md-6">
                        <label for="phone number">Date of Birth</label>
                        <input type="text" name="dob" class="form-control mydatepicker" placeholder="dd/mm/yyyy">
                    </div>

                </div>
               

                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>

            <fieldset>

                <h2 class="fs-title">Step 2</h2>

                <div class="row">

                    <div class="col-md-6">
                        <label for="phone number">Business Phone Number(required)</label>
                        <input type="text" name="phone_number" placeholder="Phone Number(Required)" required />

                    </div>

                    <div class="col-md-6">
                        <label for="phone number">Personal Phone Number(Not required)</label>
                <input type="text" name="other_number" placeholder="Phone Number" required /> 
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-6">
                      <label for="Email">Email</label>
                        <input type="email" name="email" placeholder="Email" required />  
                    </div>

                    <div class="col-md-6">
                        <label for="SCN">Enrollment Number</label>
                <input type="text" name="scn" placeholder="SCN123456AB" required />
                    </div>
                </div>   

                 <!-- <div class="row">
                    <div class="col-md-6">
                        
                    </div>
                    <div class="col-md-6">
                        
                    </div>
                </div> -->   

                <div class="row">
                    <div class="col-md-6">
                        <label for="phone number">Enrollment Year(Call to bar)</label>
                        <input type="text" name="year" class="form-control mydatepicker" placeholder="dd/mm/yyyy">
                    </div>

                    <div class="col-md-6">
                        <label for="Branch">Branch</label>
                        <select class="custom-select form-control" id="branch" name="branch" style="margin-bottom: 18px" required>
                        <option value="">Choose Branch</option>
                        <?php if(isset($branches)): ?>
                            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($branch->name); ?>"><?php echo e($branch->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </select>

                    </div>

                </div>

               
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>           
            
            <fieldset>
                <h2 class="fs-title">Step 3</h2>



                <label for="Practice Category">Practice Type</label>
                <select class="custom-select form-control" id="practice_type" name="practice_type" style="margin-bottom: 18px" required>
                    <option>Choose Category</option>
                    <option value="Private">Private</option>
                    <option value="Public">Public</option>
                </select>
                <br />

                <label for="Branch">Permanent Address</label>
                <textarea name="permanent_address" required rows="1" ></textarea>


                <label for="Branch">Office Address</label>
                <textarea name="office_address" required rows="1"></textarea>

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>

            <fieldset>
                    <h2 class="fs-title">Step 4</h2>

                <div class="row">
                    <div class="col-md-6">
                        <label for="Practice Category">Means of Identification </label>

                        <select class="custom-select form-control" id="card_type" name="card_type" style="margin-bottom: 18px" required>
                            <option>Choose Category</option>
                            <option value="NBA Affinity Card">NBA Affinity Card</option>
                            <option value="National Id">National Id</option>
                            <option value="Voter Card">Voter's Card</option>
                            <option value="Driver's License">Driver's License</option>
                            <option value="International Passport">International Passport</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                      <label for="serial number">Card Number</label>
                    <input type="text" name="card_number" placeholder="Card Number" required />
  
                    </div>
                </div>
                
                    <div class="row">
                        
                        <label for="input-file-max-fs">Upload (5M max)</label>
                        <input type="file" id="input-file-max-fs" class="dropify" name="card_image" data-max-file-size="5M" />
                                      
                    </div>

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" id="preview" class="next action-button" value="Preview" />
                </fieldset>

                <fieldset style="background-color: rgba(204, 204, 204, 0.2)">
                    <h2 class="fs-title">Confirmation page</h2>
                    <div class="row" id="final-step">
                        <div class="col-md-6" style="text-align:left;">
                            <span class="prev-row">
                            Salutation : <span id="prev_salutation" class="result"></span>  <hr>
                            </span>
                             <span class="prev-row">
                            First name : <span id="prev_first_name" class="result"></span>  <hr>
                        </span>
                         <span class="prev-row">
                            Middle Name : <span id="prev_middle_name" class="result"></span> <hr>
                            </span>
                             <span class="prev-row">
                            Last Name: <span id="prev_last_name" class="result"></span> <hr>
                            </span>
                             <span class="prev-row">
                            DOB: <span id="prev_dob" class="result"></span><hr>
                        </span>
                         <span class="prev-row">
                            Email: <span id="prev_email" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Business Phone Number: <span id="prev_phone_number" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Personal Phone Number: <span id="prev_other_number" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Gender: <span id="prev_gender" class="result"></span> <hr>
                        </span>
                         
                        </div>

                        <div class="col-md-6" style="text-align:left;">
                            <span class="prev-row">
                            Enrolment Number: <span id="prev_scn" class="result"></span> <hr>
                        </span>
                             <span class="prev-row">
                            Year: <span id="prev_year" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Practice Type: <span id="prev_practice_type" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Branch: <span id="prev_branch" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Permanent Address: <span id="prev_permanent_address" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Office Address: <span id="prev_office_address" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Card Type: <span id="prev_card_type" class="result"></span> <hr>
                        </span>
                         <span class="prev-row">
                            Card Number: <span id="prev_card_number" class="result"></span> <hr>
                        </span>
                            

                        </div>

                    </div>
                                       

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" name="submit" id="" class="action-button" value="Submit" />

                </fieldset>
        
        </form>
        <div class="clear"></div>
       
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/node_modules/dropify/dist/js/dropify.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/script.js')); ?>"></script>
    <script>
        $(document).ready(function() {
            //jQuery('.mydatepicker, #datepicker').datepicker();
            jQuery('.mydatepicker').datepicker({
        autoclose: true,
        todayHighlight: true
    });
            $('.dropify').dropify();

            $('#preview').on('click',function(){
                var myForm =  document.getElementById('msform');
                
                var formData = new FormData(myForm);

                //const finalStep = document.querySelector('#final-step') 

                for(const [key, value] of formData){
                    //if()
                    $('#prev_'+key).html(value);
                // else
                //     $('#prev_'+key).html('cannot be empty');
                }
                
            });

            // $('#practice_type').on('click',function()){
            //     var myForm =  document.getElementById('msform');
            //     var formData = new FormData(myForm);
            // }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.frontend.verification', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>