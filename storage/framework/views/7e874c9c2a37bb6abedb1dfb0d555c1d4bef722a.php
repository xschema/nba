<?php $__env->startSection('title','Login'); ?>
<?php $__env->startPush('style'); ?>
<style type="text/css">
    #wrapper{
    /* width: 100%; */
    overflow-x: scroll;
    }
</style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?> 
     <div class="login-register" style="top: -20px">

                <!-- multistep form -->
            <a href="<?php echo e(route('verification.index')); ?>" class="text-center m-b-20" style="display: block"><img src="<?php echo e(asset('img/logo.png')); ?>" alt="NBA" width="120" /></a>

            <div class="login-box card">
                <div class="card-body">
                   
                    <form class="form-horizontal form-material" id="loginform" method="post" action="<?php echo e(route('login')); ?>">
                        <?php echo csrf_field(); ?>
                        <h3 class="box-title m-b-20">Login</h3>
                        <?php if(!empty($errors->first())): ?>
                        <p class="text-danger"><?php echo e($errors->first()); ?></p>
                    <?php endif; ?>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" name="email" type="text" required="" placeholder="Email"> </div>
                                
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" name="password" required="" placeholder="Password"> </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                    <a href="<?php echo e(route('password.request')); ?>" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> 
                                </div> 
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-success btn-rounded" type="submit">Log In</button>
                            </div>
                        </div>
                        
                        <div class="form-group m-b-0">

                            <div class="col-sm-12 text-center">

                                Don't have an account? <a href="<?php echo e(route('register')); ?>" class="text-info m-l-5"><b>Register </b></a>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
$('#to-recover').on("click", function() {
    $("#loginform").slideUp();
    $("#recoverform").fadeIn();
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.auth.login', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>