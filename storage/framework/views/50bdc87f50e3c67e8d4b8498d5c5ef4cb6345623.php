<?php $__env->startSection('title','Dashboard'); ?>
<?php $__env->startPrepend('styles'); ?>
<?php
    $role = auth()->user()->role
?>

<script>
  window.userRole = `<?php echo $role; ?>`
</script>
<!-- Dashboard 1 Page CSS -->
<?php $__env->stopPrepend(); ?>

<?php $__env->startSection('content'); ?>
<!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Admin Dashboard</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div>
                            <h5 class="card-title">Stamp Request</h5>
                            <h6 class="card-subtitle">All Purchases</h6>
                        </div>
                        <div class="ml-auto">
                        </div>
                    </div>
                </div>
                <div id="app">
                    <flash></flash>
                    <stamp-requests-list> </stamp-requests-list>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<!--morris JavaScript -->
    <!-- Popup message jquery -->
    <!-- Chart JS -->
      <script src="<?php echo e(asset('js/manifest.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor.js')); ?>"></script>
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.backend.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>