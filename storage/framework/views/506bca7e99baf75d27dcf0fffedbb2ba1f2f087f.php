<?php $__env->startPush('styles'); ?>

<?php $__env->stopPush(); ?>


<?php $__env->startSection('content'); ?>
<!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo e(route('verification.index')); ?>">
          <img src="<?php echo e(asset('img/logo.png')); ?>" width="70" class="img-responsive">
         </a>

        <div class="navbar-right">
          <a href="<?php echo e(route('login')); ?>" class="btn btn-info"> Login</a>
          <a href="<?php echo e(route('register')); ?>" class="btn btn-info"> Register</a>
        </div>
        


      </div>
    </nav>

    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-5">Welcome to NBA Stamp Purchase Portal</h2>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
          <form method="post" id="search" action="<?php echo e(route('nba.show')); ?>">
                <?php echo csrf_field(); ?>
              <div class="form-row" style="">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                    
                  <input type="text" name="scn" id="scn" class="form-control form-control-lg"  placeholder="Enter your SCN (SCN123456)..." required="required" />
                </div>
                <div class="col-12 col-md-3">
                  <button type="submit" class="btn btn-block btn-lg btn-default search"><i class="glyphicon glyphicon-search"></i> Submit</button>
                </div>
              </div>
			     </form>

      			  <div class="col-12 result" style="display:none">
      				  <div style=" margin:auto;">
                  <h4 id="lawyer-name"> </h4>
      					  <div style="margin-top: 20px;">
                    <a id="continue" class="btn btn-info pull-right" style="display:none; margin-right: 20px;"> Continue</a>
                  <a href="<?php echo e(route('register')); ?>" id="contact" class="btn pull-right" style="margin-right: 20px; background-color: #ff9118; color: white;"> Contact Support</a>
                  
                  <button class="btn btn-danger close-result"> Cancel</button>
                  </div>
      				</div>
      			</div>
          </div>
        </div>
      </div>
    </header>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
    $(document).ready(function(){



  $('#continue').on('click',function(){
    window.location = $(this).attr('href');
  })

	$('#search').on('submit',function(e){
    e.preventDefault();
    $('.search').text('Processing...').attr('disabled', true);
    //loadingform
    $form = $(this);
    var action = $form.attr('action');
    var formData =$form.serialize();
    $.ajax({
        type: 'POST',
        url : action,
        data : formData,
        success : function(data){
            if(!data.error){
                //console.log(data['first_name']);

                var name = data['first_name'] +' '+data['middle_name']+' '+ data['last_name'];
                var scn =  data['scn'];
                $('#contact').hide();
                $('#continue').attr('href', `verification/${data['scn']}`).show();

                //$('#lawyer-name').empty();

               showSearchResult(name);
                //no record found
            }else{
              //data=[];
              $('#continue').hide()
              $('#contact').show();
                showSearchResult('No record found');
            }
        },
        error: function(){
            showSearchResult('Error Occured, Please try again !!');
        }
    });

});

function showSearchResult(result){
  $('#lawyer-name').html(result);
  $('.form-row').hide('slow');
  $('.result').show('slow');
}

$('.close-result').on('click',function(){
  $('#search')[0].reset();
   $('.search').text('Submit').attr('disabled', false);

  $('.result').hide('slow');
  $('.form-row').show('slow');
});

});//end document.ready

//$('#continue').on('click')
    //window.location =


</script>
<?php $__env->stopPush(); ?>




<?php echo $__env->make('layouts.frontend.landing', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>