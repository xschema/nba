<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' =>'Idris',
            'middle_name' =>'Adamu',
            'last_name' =>'Adamu',
            'email' =>'manager@nbaportal.org.ng',
            'password' => bcrypt('secret'),
            'role' =>'manager',
            'avatar_url' =>'https://www.gravatar.com/avatar/'. md5( strtolower( 'manager@nbaportal.org.ng' ) ).'?d=mp',
        ]);

        DB::table('users')->insert([
            'first_name' =>'Peter',
            'middle_name' =>'Frank',
            'last_name' =>'Abu',
            'email' =>'support@nbaportal.org.ng',
            'password' => bcrypt('secret'),
            'role' =>'support',
            'avatar_url' =>'https://www.gravatar.com/avatar/'. md5( strtolower( 'support@nbaportal.org.ng' ) ).'?d=mp',
        ]);

        DB::table('users')->insert([
            'first_name' =>'Petra',
            'middle_name' =>'Ann',
            'last_name' =>'Damilola',
            'email' =>'lawyer@nbaportal.org.ng',
            'password' => bcrypt('secret'),
            'role' =>'lawyer',
            'avatar_url' =>'https://www.gravatar.com/avatar/'. md5( strtolower( 'lawyer@nbaportal.org.ng' ) ).'?d=mp',
        ]);

        DB::table('lawyers')->insert([
        'scn'=> 'SCN123456',
         'phone_number' => '07012345678',
         'branch' => 'ABUJA',
        'email' => 'lawyer@nbaportal.org.ng',
        'user_id' => 3,
        'category'=>'Public',
        ]);

          if(app()->env  == 'local'){
            factory('App\User', 1)->create(['email' => 'meyfemi@nba.org'])
            ->each(function ($user) {
              $user->lawyer_account()->save(factory(App\Lawyer::class)->make());
            });
          }


        factory('App\Ticket', 2)->create();

        factory(App\StampRequest::class, 4)->create()
                ->each(function( $request ) {
                    App\User::first()->stampRequests()->save($request);
          });
    }
}
