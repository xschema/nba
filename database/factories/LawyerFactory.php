<?php

use Faker\Generator as Faker;

$factory->define(App\Lawyer::class, function (Faker $faker) {

    $pre="SCN";
    return [
        'scn'=> $pre.$faker->numberBetween(1111, 999999),
         'phone_number' => $faker->phoneNumber,
         'branch' => $faker->city,
        'email' => $faker->unique()->safeEmail,
        'user_id' => 1,
        'category'=>$faker->randomElement(array('Public','Private' )),
    ];
});
