<?php

use Faker\Generator as Faker;
 
$factory->define(App\NbaDb::class, function (Faker $faker) {
    $date = $faker->dateTimeBetween($startDate = '-37 years', $endDate = 'now', $timezone = null);
    $num = $faker->unique()->randomNumber(6);
    return [
        'scn'=> 'SCN'.$num,
         'first_name' => $faker->firstName,
         'middle_name' => $faker->firstName,
         'last_name' => $faker->lastName,
         'phone_number' => $faker->phoneNumber,
         'branch' => $faker->city,
        'email' => $faker->unique()->safeEmail,
        'year'=> date_format($date, 'Y')
    ];
});
