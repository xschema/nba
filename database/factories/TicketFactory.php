<?php

use Faker\Generator as Faker;

$factory->define(App\Ticket::class, function (Faker $faker) {
    return [

        'user_id' => $faker->numberBetween(1,5),
        'ticket_number' => $faker->unique()->randomNumber(6),
        'subject' => $faker->text(50,100),
        'message' => $faker->text(150,300),
        'status' => $faker->randomElement(array('Pending','Answered','Closed')),
    ];
});
