<?php

use App\StampRequest;
use Faker\Generator as Faker;


$factory->define(StampRequest::class, function (Faker $faker) {
    return [
      'buyer_id' => function () {
            return factory(App\User::class)->create()->id;
        },
      'type' =>$faker->randomElement(array('Private', 'Public')),
      'expiry' =>'new',
      'packs' => $faker->numberBetween(1,10),
      'sheets' => $faker->numberBetween(1,3),
      'amount' => $faker->numberBetween(150000, 5000000),
      'charges' => $faker->numberBetween(100, 500),
      'paid' => false,
      'status' => 'unconfirmed',
      'pick_up_address' => $faker->randomElement(array('Some Address', 'Branch Office'))
    ];
});
