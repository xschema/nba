<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stamp_requests', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('buyer_id');
            $table->string('transaction_id')->nullable();
            $table->enum('type', ['Private', 'Public']);
            $table->enum('expiry', ['old', 'new']);
            $table->integer('packs');
            $table->integer('amount');
            $table->integer('charges');
            $table->integer('sheets');
            $table->boolean('paid')->default(0);
            $table->enum('status', ['unconfirmed', 'confirmed', 'processing', 'shipped', 'delivered'])->default('unconfirmed');
            $table->string('pick_up_address');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stamp_requests');
    }
}
