<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('salutation');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->date('dob');
            $table->string('email');
            $table->string('gender');
            $table->string('phone_number');
            $table->string('other_number')->nullable();
            $table->string('scn');
            $table->string('year');
            $table->string('practice_type');
            $table->string('branch');
            $table->string('permanent_address');
            $table->string('office_address')->nullable();
            $table->string('card_type');
            $table->string('card_number');
            $table->string('card_image');
            $table->enum('status',['Pending','Approved','Rejected'])->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
